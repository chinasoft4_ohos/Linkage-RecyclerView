/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kunminx.linkage.materialripple;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.utils.Color;

import java.util.NoSuchElementException;

final class TypedAttrUtils {

    public static int getIntColor(AttrSet attrs, String attrName, int defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getColorValue().getValue();
        }
    }

    public static Color getColor(AttrSet attrs, String attrName, Color defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getColorValue();
        }
    }

    public static boolean getBoolean(AttrSet attrs, String attrName, boolean defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getBoolValue();
        }
    }

    public static String getString(AttrSet attrs, String attrName, String defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getStringValue();
        }
    }

    public static int getDimensionPixelSize(AttrSet attrs, String attrName, int defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getIntegerValue();
        }
    }

    public static int getLayoutDimension(AttrSet attrs, String attrName, int defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getDimensionValue();
        }
    }


    public static float getFloat(AttrSet attrs, String attrName, float defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getFloatValue();
        }
    }

    public static int getInteger(AttrSet attrs, String attrName, int defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getIntegerValue();
        }
    }

    private static Attr attrNoSuchElement(AttrSet attrs, String attrName) {
        Attr attr = null;
        try {
            attr = attrs.getAttr(attrName).get();
        } catch (NoSuchElementException e) {
        }
        return attr;
    }
}
