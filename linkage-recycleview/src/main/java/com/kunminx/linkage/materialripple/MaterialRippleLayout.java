/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kunminx.linkage.materialripple;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

public class MaterialRippleLayout extends StackLayout implements Component.TouchEventListener,
        Component.DrawTask,
        Component.ClickedListener {

    private Component childView;

    private static final boolean DEFAULT_DELAY_CLICK = true;
    private static final float DEFAULT_ALPHA = 0.2f;
    private static final int DEFAULT_DURATION = 350;
    private static final float DEFAULT_DIAMETER_DP = 720;//默认的圆半径
    private static final int HOVER_DURATION = 1500;//悬停时长
    //    private static final int DEFAULT_RADIUS = 80;
    private static final int ADD_RADIUS_LENGTH = 20;
    private Color rippleColor = Color.BLACK;
    private float rippleAlpha = DEFAULT_ALPHA;
    public int rippleDuration;
    private float rippleDiameter = DEFAULT_DIAMETER_DP;
    private static final boolean DEFAULT_RIPPLE_OVERLAY = false;
    private boolean rippleOverlay;
    private boolean rippleInAdapter;

    private static final Color DEFAULT_BACKGROUND = Color.TRANSPARENT;
    //默认水波纹颜色
    private static final int DEFAULT_RIPPLE_COLOR = 0x80000000;

    private MmiPoint pointerPosition;
    private DelayRunAnimator delayRunAnimator = new DelayRunAnimator();
    private EventHandler handler;
    // 动画
    private AnimatorValue animatorValue;

    //    private RippleCircleView rippleCircleView;
    private ListContainer parentAdapter;

    private boolean isLongClick = false;

    private float maxRadius;

    private float mRadiusRipple;
    //手势是否抬起
    private boolean mFingerUp = true;
    private float x;
    private float y;
    private final Paint mPaint = new Paint();

    public MaterialRippleLayout(Context context) {
        super(context);
        init(context, null, "");
    }

    public MaterialRippleLayout(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(context, attrSet, "");
    }

    public MaterialRippleLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(context, attrSet, styleName);
    }

    public static RippleBuilder on(Component view) {
        return new RippleBuilder(view);
    }

    private void init(Context context, AttrSet attrs, String defStyleAttr) {
        handler = new EventHandler(EventRunner.getMainEventRunner());

        initAttrs(context, attrs, defStyleAttr);
        setupPaint();

        addDrawTask(this::onDraw);
        setTouchEventListener(this::onTouchEvent);
    }

    private void initAttrs(Context context, AttrSet attrSet, String defStyleAttr) {
        if (null == attrSet) {
            return;
        }

        rippleColor = TypedAttrUtils.getColor(attrSet, "mrl_rippleColor", new Color(DEFAULT_RIPPLE_COLOR));
        rippleAlpha = TypedAttrUtils.getFloat(attrSet, "mrl_rippleAlpha", DEFAULT_ALPHA);
        rippleDuration = TypedAttrUtils.getInteger(attrSet, "mrl_rippleDuration", DEFAULT_DURATION);
        rippleDiameter = TypedAttrUtils.getFloat(attrSet, "mrl_rippleDiameterDp", DEFAULT_DIAMETER_DP);
        rippleOverlay = TypedAttrUtils.getBoolean(attrSet, "mrl_rippleOverlay", DEFAULT_RIPPLE_OVERLAY);
    }

    /**
     * 初始化画笔
     */
    private void setupPaint() {
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        mPaint.setColor(rippleColor);
        mPaint.setAlpha(rippleAlpha);
    }

    private ShapeElement mOuterLineDrawable;

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (null != mOuterLineDrawable) {
            mOuterLineDrawable.drawToCanvas(canvas);
        }

        canvas.drawCircle(getX(), getY(), getRadius(), mPaint);
    }

    private static final int DEFAULT_OUTER_LINE_COLOR = 0x1A000000;

    private ShapeElement generateBackgroundDrawable() {
        ShapeElement element = new ShapeElement();
        element.setRgbColor(RgbColor.fromArgbInt(DEFAULT_OUTER_LINE_COLOR));
        return element;
    }

    private ShapeElement generateTransparentBackgroundDrawable() {
        ShapeElement element = new ShapeElement();
        element.setRgbColor(RgbColor.fromArgbInt(Color.TRANSPARENT.getValue()));
        return element;
    }

    // 动画侦听函数
    private final AnimatorValue.ValueUpdateListener mAnimatorUpdateListener
            = new AnimatorValue.ValueUpdateListener() {
        @Override
        public void onUpdate(AnimatorValue animatorValue, float v) {
            if (v <= 0) {
                return;
            }
            float currentRadius = rippleDiameter + maxRadius * v;
            if (currentRadius < rippleDiameter) {
                setRadius(rippleDiameter);
            } else {
                setRadius(currentRadius);
            }

            invalidate();
        }
    };

    /**
     * 点击事件的动画
     */
    private void initAnimator() {
        animatorValue = new AnimatorValue();
        animatorValue.setCurveType(Animator.CurveType.ACCELERATE_DECELERATE);
        animatorValue.setDuration(rippleDuration);
        animatorValue.setValueUpdateListener(mAnimatorUpdateListener);
        animatorValue.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                mOuterLineDrawable = generateTransparentBackgroundDrawable();
//                rippleCircleView.setRadius(0);
                setRadius(0);
                fingerUp();
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        animatorValue.start();
    }

    /**
     * 长按事件动画
     */
    private void initLongClickAnimator() {
        animatorValue = new AnimatorValue();
        animatorValue.setCurveType(Animator.CurveType.LINEAR);
        animatorValue.setDuration(HOVER_DURATION);
        animatorValue.setValueUpdateListener(mAnimatorUpdateListener);
        animatorValue.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                /*mOuterLineDrawable = generateTransparentBackgroundDrawable();
                if (rippleOverlay && mFingerUp) {
                    setRadius(0);
                    invalidate();
                }
                if (!rippleOverlay) {
                    setRadius(0);
                    invalidate();
                }*/
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        animatorValue.start();
    }

    @SuppressWarnings("unchecked")
    public <T extends Component> T getChildView() {
        return (T) childView;
    }

    @Override
    public void addComponent(Component childComponent) {
        if (getChildCount() > 0) {
            throw new IllegalStateException("MaterialRippleLayout can host only one child");
        }
        //noinspection unchecked
        childView = childComponent;
        super.addComponent(childComponent);
    }


    @Override
    public void setClickedListener(ClickedListener listener) {
        if (childView == null) {
            throw new IllegalStateException("MaterialRippleLayout must have a child view to handle clicks");
        }
        childView.setClickedListener(listener);
    }

    @Override
    public void setLongClickedListener(LongClickedListener listener) {
        if (childView == null) {
            throw new IllegalStateException("MaterialRippleLayout must have a child view to handle clicks");
        }
        childView.setLongClickedListener(listener);
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getRadius() {
        return mRadiusRipple;
    }

    public void setRadius(float radius) {
        this.mRadiusRipple = radius;
    }


    @Override
    public void onClick(Component component) {
        isLongClick = false;
    }


    //maxRadius 通过动态计算 maxRadius  的值能缓解波纹扩散后悬停的时长
    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        System.out.println("touch event : " + touchEvent.getAction());
        touchEvent.getRadius(0);
        //获取坐标点
        pointerPosition = touchEvent.getPointerPosition(0);

        float x = pointerPosition.getX();
        float y = pointerPosition.getY();

        int[] parentLocationOnScreen = getLocationOnScreen();

        setX(x - parentLocationOnScreen[0]);
        setY((float) (component.getHeight() / 2d));
        switch (touchEvent.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                mOuterLineDrawable = generateBackgroundDrawable();

                mFingerUp = false;
                handler.postTask(delayRunAnimator, 200);
                setRippleDuration(rippleDuration);

                if (Math.abs((x - parentLocationOnScreen[0])) > Math.abs(x - (parentLocationOnScreen[0] + getWidth()))) {
                    maxRadius = Math.abs((x - parentLocationOnScreen[0])) + ADD_RADIUS_LENGTH;
                } else {
                    maxRadius = Math.abs(x - (parentLocationOnScreen[0] + getWidth())) + ADD_RADIUS_LENGTH;
                }

                mRadiusRipple = maxRadius;
                //按下事件
                fingerPress();
                resetConfig();
                return true;

            case TouchEvent.POINT_MOVE:
                //移动事件
                break;

            case TouchEvent.PRIMARY_POINT_UP:
                //抬起事件
                mFingerUp = true;

                if (!isLongClick) {
                    handler.removeTask(delayRunAnimator);
                    if (rippleDuration <= 0) {
                        rippleDuration = DEFAULT_DURATION;
                    }
                    setRippleDuration(rippleDuration);
                    initAnimator();
                }

                if (isLongClick) {
                    fingerUp();
                }

                break;
        }
        return false;
    }

    private class DelayRunAnimator implements Runnable {

        @Override
        public void run() {
            isLongClick = true;
            initLongClickAnimator();
        }
    }

    /**
     * 点击操作
     */
    private void performalViewClick() {
        // if parent is an AdapterView, try to call its ItemClickListener
        if (getComponentParent() instanceof ListContainer) {
            // try clicking direct child first
            if (!childView.callOnClick()) {
                // if it did not handle it dispatch to adapterView
                clickAdapterView((ListContainer) getComponentParent());
            }

        } else if (rippleInAdapter) {
            // find adapter view
            clickAdapterView(findParentAdapterView());
        } else {
            // otherwise, just perform click on child
            if (null != childView) {
                childView.callOnClick();
            }
        }
    }

    private void clickAdapterView(ListContainer parent) {
        final int position = parent.getIndexForComponent(MaterialRippleLayout.this);
        final long itemId = parent.getItemProvider() != null
                ? parent.getItemProvider().getItemId(position)
                : 0;
        if (position != ListContainer.INVALID_INDEX) {
            parent.executeItemClick(MaterialRippleLayout.this, position, itemId);
        }
    }

    /*
     * Helper
     */
    private class PerformClickEvent implements Runnable {

        @Override
        public void run() {
//            if (hasPerformedLongPress) return;
            // if parent is an AdapterView, try to call its ItemClickListener
            if (getComponentParent() instanceof ListContainer) {
                // try clicking direct child first
                if (!childView.callOnClick()) {
                    // if it did not handle it dispatch to adapterView
                    clickAdapterView((ListContainer) getComponentParent());
                }

            } else if (rippleInAdapter) {
                // find adapter view
                clickAdapterView(findParentAdapterView());
            } else {
                // otherwise, just perform click on child
                if (null != childView) {
                    childView.callOnClick();
                }
                callOnClick();
            }
        }


        private void clickAdapterView(ListContainer parent) {
            final int position = parent.getIndexForComponent(MaterialRippleLayout.this);
            final long itemId = parent.getItemProvider() != null
                    ? parent.getItemProvider().getItemId(position)
                    : 0;
            if (position != ListContainer.INVALID_INDEX) {
//                parent.executeItemClick(RippleView.this, position, itemId);
                parent.executeItemClick(MaterialRippleLayout.this, position, itemId);
            }
        }
    }

    private ListContainer findParentAdapterView() {
        if (parentAdapter != null) {
            return parentAdapter;
        }
        ComponentParent current = getComponentParent();
        while (true) {
            if (current instanceof ListContainer) {
                parentAdapter = (ListContainer) current;
                return parentAdapter;
            } else {
                if (current != null) {
                    current = current.getComponentParent();
                }
            }
        }
    }

    private void resetConfig() {
        setRadius(getRadius());
    }

    /**
     * 按下
     */
    private void fingerPress() {
        if (null != childView) {
            return;
        }
    }

    /**
     * 抬起
     */
    private void fingerUp() {
        if (rippleOverlay && mFingerUp) {
            setRadius(0);
            invalidate();
        }

        mOuterLineDrawable = generateTransparentBackgroundDrawable();
        if (rippleOverlay && mFingerUp) {
            setRadius(0);
            invalidate();
        }
        if (!rippleOverlay) {
            setRadius(0);
            invalidate();
        }

        isLongClick = false;
    }

    /*
     * Accessor
     */
    public void setRippleColor(int rippleColor) {
        this.rippleColor = new Color(rippleColor);
        mPaint.setColor(new Color(rippleColor));
        mPaint.setAlpha(rippleAlpha);
        invalidate();
    }

    /**
     * 设置透明度
     *
     * @param alpha 透明度
     */
    public void setDefaultRippleAlpha(float alpha) {
        this.rippleAlpha = alpha;
        mPaint.setAlpha(rippleAlpha);
        invalidate();
    }

    public void setRippleDiameter(int rippleDiameter) {
        this.rippleDiameter = rippleDiameter;
    }

    public void setRippleDuration(int rippleDuration) {
        this.rippleDuration = rippleDuration;
    }

    public void setRippleOverlay(boolean overlay) {
        this.rippleOverlay = overlay;
    }

    public void setRippleBackground(RgbColor color) {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(color);
        setBackground(shapeElement);
    }


    /*
     * Builder
     */
    public static class RippleBuilder {
        private final Context context;
        private final Component child;

        private int rippleColor = Color.rgb(0, 0, 0);
        private boolean rippleOverlay = DEFAULT_RIPPLE_OVERLAY;
        private float rippleDiameter = DEFAULT_DIAMETER_DP;
        private int rippleDuration = DEFAULT_DURATION;
        private float rippleAlpha = DEFAULT_ALPHA;
        private int rippleBackground = Color.rgb(255, 255, 255);

        public RippleBuilder(Component child) {
            this.child = child;
            this.context = child.getContext();
        }

        public RippleBuilder rippleColor(int color) {
            this.rippleColor = color;
            return this;
        }

        public RippleBuilder rippleOverlay(boolean overlay) {
            this.rippleOverlay = overlay;
            return this;
        }

        public RippleBuilder rippleDiameterDp(int diameterDp) {
            this.rippleDiameter = diameterDp;
            return this;
        }

        public RippleBuilder rippleDuration(int duration) {
            this.rippleDuration = duration;
            return this;
        }

        public RippleBuilder rippleAlpha(float alpha) {
            this.rippleAlpha = alpha;
            return this;
        }


        public RippleBuilder rippleBackground(int color) {
            this.rippleBackground = color;
            return this;
        }


        public MaterialRippleLayout create() {
            MaterialRippleLayout layout = new MaterialRippleLayout(context);
            layout.setRippleColor(rippleColor);
            layout.setDefaultRippleAlpha(rippleAlpha);
            layout.setRippleDiameter(AttrHelper.fp2px(rippleDiameter, context));
            layout.setRippleDuration(rippleDuration);
            layout.setRippleOverlay(rippleOverlay);
            layout.setRippleBackground(new RgbColor(rippleBackground));

            ShapeElement shapeElement = new ShapeElement();
            shapeElement.setRgbColor(RgbColor.fromArgbInt(rippleBackground));
            child.setBackground(shapeElement);

            ComponentContainer.LayoutConfig params = child.getLayoutConfig();
            ComponentContainer parent = (ComponentContainer) child.getComponentParent();
            int index = 0;

            if (parent != null && parent instanceof MaterialRippleLayout) {
                throw new IllegalStateException("MaterialRippleLayout could not be created: parent of the view already is a MaterialRippleLayout");
            }

            if (parent != null) {
                index = parent.getChildIndex(child);//.indexOfChild(child);
                parent.removeComponent(child);//.removeView(child);
            }

            layout.addComponent(child, new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT));

            if (parent != null) {
                parent.addComponent(layout, index, params);
//                parent.addView(layout, index, params);
            }

            return layout;
        }
    }

}
