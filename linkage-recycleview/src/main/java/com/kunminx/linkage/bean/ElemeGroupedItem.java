/*
 * Copyright (c) 2018-present. KunMinX
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kunminx.linkage.bean;

import java.util.List;

/**
 * Create by KunMinX at 19/4/27
 */
public class ElemeGroupedItem {
    /**
     * code : 1
     * data : [{"isHeader":true,"info":{"content":"","group":"优惠","imgUrl":"","title":""}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"优惠","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png","title":"全家桶"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"优惠","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png","title":"可乐"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"优惠","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-4a6889ba228feb46.png","title":"小食四拼"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"优惠","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-636381a9f7a855d6.png","title":"烤全翅"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"优惠","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-203db4255e78922a.png","title":"奥尔良鸡腿汉堡"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"优惠","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png","title":"鳕鱼堡"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"优惠","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png","title":"鸡腿"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"优惠","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-203db4255e78922a.png","title":"草莓派"}},{"isHeader":true,"info":{"content":"","group":"套餐","imgUrl":"","title":""}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png","title":"全家桶"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png","title":"可乐"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-4a6889ba228feb46.png","title":"小食四拼"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-636381a9f7a855d6.png","title":"烤全翅"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-203db4255e78922a.png","title":"奥尔良鸡腿汉堡"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png","title":"鳕鱼堡"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png","title":"鸡腿"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-203db4255e78922a.png","title":"草莓派"}},{"isHeader":true,"info":{"content":"","group":"主食","imgUrl":"","title":""}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"主食","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png","title":"全家桶"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"主食","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png","title":"可乐"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"主食","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-4a6889ba228feb46.png","title":"小食四拼"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"主食","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-636381a9f7a855d6.png","title":"烤全翅"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"主食","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-203db4255e78922a.png","title":"奥尔良鸡腿汉堡"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"主食","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png","title":"鳕鱼堡"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"主食","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png","title":"鸡腿"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"主食","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-203db4255e78922a.png","title":"草莓派"}},{"isHeader":true,"info":{"content":"","group":"饮料","imgUrl":"","title":""}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"饮料","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png","title":"全家桶"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"饮料","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png","title":"可乐"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"饮料","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-4a6889ba228feb46.png","title":"小食四拼"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"饮料","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-636381a9f7a855d6.png","title":"烤全翅"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"饮料","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-203db4255e78922a.png","title":"奥尔良鸡腿汉堡"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"饮料","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png","title":"鳕鱼堡"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"饮料","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png","title":"鸡腿"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"饮料","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-203db4255e78922a.png","title":"草莓派"}},{"isHeader":true,"info":{"content":"","group":"套餐A","imgUrl":"","title":""}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐A","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png","title":"全家桶"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐A","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png","title":"可乐"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐A","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-4a6889ba228feb46.png","title":"小食四拼"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐A","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-636381a9f7a855d6.png","title":"烤全翅"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐A","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-203db4255e78922a.png","title":"奥尔良鸡腿汉堡"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐A","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png","title":"鳕鱼堡"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐A","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png","title":"鸡腿"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐A","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-203db4255e78922a.png","title":"草莓派"}},{"isHeader":true,"info":{"content":"","group":"套餐B","imgUrl":"","title":""}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐B","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png","title":"全家桶"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐B","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png","title":"可乐"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐B","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-4a6889ba228feb46.png","title":"小食四拼"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐B","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-636381a9f7a855d6.png","title":"烤全翅"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐B","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-203db4255e78922a.png","title":"奥尔良鸡腿汉堡"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐B","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png","title":"鳕鱼堡"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐B","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png","title":"鸡腿"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐B","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-203db4255e78922a.png","title":"草莓派"}},{"isHeader":true,"info":{"content":"","group":"套餐C","imgUrl":"","title":""}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐C","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png","title":"全家桶"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐C","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png","title":"可乐"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐C","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-4a6889ba228feb46.png","title":"小食四拼"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐C","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-636381a9f7a855d6.png","title":"烤全翅"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐C","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-203db4255e78922a.png","title":"奥尔良鸡腿汉堡"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐C","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png","title":"鳕鱼堡"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐C","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png","title":"鸡腿"}},{"isHeader":false,"info":{"content":"好吃的食物，增肥神器，有求必应\n月售10008 好评率100%","group":"套餐C","imgUrl":"https://upload-images.jianshu.io/upload_images/57036-203db4255e78922a.png","title":"草莓派"}}]
     */
    private String code;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * isHeader : true
         * info : {"content":"","group":"优惠","imgUrl":"","title":""}
         */

        private boolean isHeader;
        private InfoBean info;

        public boolean isIsHeader() {
            return isHeader;
        }

        public void setIsHeader(boolean isHeader) {
            this.isHeader = isHeader;
        }

        public InfoBean getInfo() {
            return info;
        }

        public void setInfo(InfoBean info) {
            this.info = info;
        }

        public static class InfoBean {
            /**
             * content :
             * group : 优惠
             * imgUrl :
             * title :
             */

            private String content;
            private String group;
            private String imgUrl;
            private String title;

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getGroup() {
                return group;
            }

            public void setGroup(String group) {
                this.group = group;
            }

            public String getImgUrl() {
                return imgUrl;
            }

            public void setImgUrl(String imgUrl) {
                this.imgUrl = imgUrl;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }
        }
    }
}
