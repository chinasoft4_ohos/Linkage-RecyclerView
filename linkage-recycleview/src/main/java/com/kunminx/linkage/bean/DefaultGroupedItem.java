/*
 * Copyright (c) 2018-present. KunMinX
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kunminx.linkage.bean;

/**
 * Create by KunMinX at 19/4/27
 */
public class DefaultGroupedItem extends BaseGroupedItem<DefaultGroupedItem.ItemInfo> {
    /**
     * DefaultGroupedItem
     *
     * @param isHeader isHeader
     * @param header   header
     */
    public DefaultGroupedItem(boolean isHeader, String header) {
        super(isHeader, header);
    }

    /**
     * DefaultGroupedItem
     *
     * @param item item
     */
    public DefaultGroupedItem(ItemInfo item) {
        super(item);
    }

    public static class ItemInfo extends BaseGroupedItem.ItemInfo {
        private String content;

        /**
         * ItemInfo
         *
         * @param title   title
         * @param group   group
         * @param content content
         */
        public ItemInfo(String title, String group, String content) {
            super(title, group);
            this.content = content;
        }

        /**
         * ItemInfo
         *
         * @param title title
         * @param group group
         */
        public ItemInfo(String title, String group) {
            super(title, group);
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }
}
