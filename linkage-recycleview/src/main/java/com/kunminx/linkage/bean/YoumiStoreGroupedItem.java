/*
 * Copyright (c) 2018-present. KunMinX
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kunminx.linkage.bean;

/**
 * Create by KunMinX at 19/4/27
 */
public class YoumiStoreGroupedItem extends BaseGroupedItem<YoumiStoreGroupedItem.ItemInfo> {
    /**
     * YoumiStoreGroupedItem
     *
     * @param isHeader header
     * @param header   头
     */
    public YoumiStoreGroupedItem(boolean isHeader, String header) {
        super(isHeader, header);
    }

    /**
     * YoumiStoreGroupedItem
     *
     * @param item 条目
     */
    public YoumiStoreGroupedItem(ItemInfo item) {
        super(item);
    }

    public static class ItemInfo extends BaseGroupedItem.ItemInfo {
        private String content;
        private String imgUrl;
        private String cost;

        /**
         * ItemInfo
         *
         * @param title   title
         * @param group   group
         * @param content 上下文
         */
        public ItemInfo(String title, String group, String content) {
            super(title, group);
            this.content = content;
        }

        /**
         * ItemInfo
         *
         * @param title   title
         * @param group   group
         * @param content 上下文
         * @param imgUrl  图片地址
         */
        public ItemInfo(String title, String group, String content, String imgUrl) {
            this(title, group, content);
            this.imgUrl = imgUrl;
        }

        /**
         * ItemInfo
         *
         * @param title   title
         * @param group   group
         * @param content 上下文
         * @param imgUrl  图片地址
         * @param cost    cost
         */
        public ItemInfo(String title, String group, String content, String imgUrl, String cost) {
            this(title, group, content, imgUrl);
            this.cost = cost;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }

        public String getCost() {
            return cost;
        }

        public void setCost(String cost) {
            this.cost = cost;
        }
    }
}
