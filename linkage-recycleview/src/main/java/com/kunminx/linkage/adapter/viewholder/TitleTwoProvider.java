/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kunminx.linkage.adapter.viewholder;

import com.kunminx.linkage.ResourceTable;
import com.kunminx.linkage.bean.ElemeGroupedItem;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.*;

import java.util.List;

public class TitleTwoProvider extends RecycleItemProvider {
    private List<ElemeGroupedItem.DataBean> list;
    private AbilitySlice slice;
    private boolean isHide;

    /**
     * TitleProvider
     *
     * @param stringList 集合
     * @param slice 上下文
     * @param isHide 是否隐藏
     */
    public TitleTwoProvider(List<ElemeGroupedItem.DataBean> stringList, AbilitySlice slice, boolean isHide) {
        this.list = stringList;
        this.slice = slice;
        this.isHide = isHide;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        Component cpt = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_default_adapter_linkage_primary, null, false);
        ElemeGroupedItem.DataBean dataBean = list.get(i);
        Text tv_title = (Text) cpt.findComponentById(ResourceTable.Id_tv_group);
        DirectionalLayout dl_title_list = (DirectionalLayout) cpt.findComponentById(ResourceTable.Id_dl_title_list);
        tv_title.setText(dataBean.getInfo().getGroup());
        if (isHide) {
            if (i == list.size() - 1) {
                dl_title_list.setVisibility(Component.HIDE);
            }
        }
        return cpt;
    }
}
