/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kunminx.linkage.adapter.viewholder;

import com.kunminx.linkage.ResourceTable;
import com.kunminx.linkage.bean.ElemeGroupedItem;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.*;
import ohos.agp.utils.Color;

import java.util.ArrayList;
import java.util.List;

public class BigProvider extends RecycleItemProvider {
    private List<ElemeGroupedItem.DataBean> list;
    private List<ElemeGroupedItem.DataBean> dataBeanList;
    private AbilitySlice slice;
    private int NumBer;
    private boolean isColor;

    /**
     * BigProvider
     *
     * @param bean 实体类
     * @param stringList 集合
     * @param slice 上下文
     * @param NumBer 数量
     */
    public BigProvider(List<ElemeGroupedItem.DataBean> bean, List<ElemeGroupedItem.DataBean> stringList, AbilitySlice slice,int NumBer,boolean isColor) {
        this.list = stringList;
        this.dataBeanList = bean;
        this.slice = slice;
        this.NumBer = NumBer;
        this.isColor = isColor;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        Component cpt = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_default_adapter_linkage_primary_two, null, false);
        ElemeGroupedItem.DataBean dataBean = list.get(i);
        if (i == list.size() - 1){
            cpt.findComponentById(ResourceTable.Id_tv_group).setVisibility(Component.HIDE);
            cpt = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_default_adapter_linkage_secondary_footer, null, false);
            Text text = (Text) cpt.findComponentById(ResourceTable.Id_tv_secondary_footer);
            if (isColor){
                text.setTextColor(Color.BLACK);
            }
        }else {
            if (dataBean.isIsHeader()) {
                Text tv_title = (Text) cpt.findComponentById(ResourceTable.Id_tv_group);
                tv_title.setText(dataBean.getInfo().getGroup());

                List<ElemeGroupedItem.DataBean> beans = new ArrayList<>();
                for (ElemeGroupedItem.DataBean bean : dataBeanList) {
                    if (bean.getInfo().getGroup().equals(dataBean.getInfo().getGroup()) && !bean.isIsHeader()) {
                        beans.add(bean);
                    }
                }

                ListContainer ls = (ListContainer) cpt.findComponentById(ResourceTable.Id_smart_list);
                TableLayoutManager tableLayoutManager = new TableLayoutManager();
                tableLayoutManager.setColumnCount(NumBer);
                tableLayoutManager.setOrientation(ListContainer.HORIZONTAL);
                ls.setLayoutManager(tableLayoutManager);
                SmartProvider smartProvider = new SmartProvider(beans, slice.getContext(), true,NumBer);
                ls.setItemProvider(smartProvider);
            }
        }
        return cpt;
    }
}
