/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kunminx.linkage.adapter.viewholder;

import com.kunminx.linkage.ResourceTable;
import com.kunminx.linkage.bean.ElemeGroupedItem;
import com.squareup.picasso.Picasso;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.List;

public class SmartProvider extends BaseItemProvider {
    private List<ElemeGroupedItem.DataBean> list;
    private Component cpt;
    private Context context;
    private boolean isShow;
    private int Number;

    /**
     * SmartProvider
     *
     * @param bean 实体类
     * @param context 上下文
     * @param isShow 是否显示
     * @param Number 数量
     */
    public SmartProvider(List<ElemeGroupedItem.DataBean> bean, Context context, boolean isShow,int Number) {
        this.list = bean;
        this.context = context;
        this.isShow = isShow;
        this.Number = Number;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        ElemeGroupedItem.DataBean dataBean = list.get(i);
        if (Number == 1){
            cpt = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_default_adapter_linkage_secondary_linear, null, false);
            Text tv_content = (Text) cpt.findComponentById(ResourceTable.Id_level_2_content);
            tv_content.setText(dataBean.getInfo().getContent());
        } else {
            cpt = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_default_adapter_linkage_secondary_grid, null, false);
        }
        Text tv_title = (Text) cpt.findComponentById(ResourceTable.Id_level_2_title);
        Image iv_foot = (Image) cpt.findComponentById(ResourceTable.Id_iv_foot);
        if (isShow) {
            Picasso.get()
                    .load(list.get(i).getInfo().getImgUrl())
                    .placeholder(ResourceTable.Media_icon)
                    .error(ResourceTable.Media_icon)
                    .fit()
                    .tag(context)
                    .into(iv_foot);
            tv_title.setTextColor(Color.BLACK);
        } else {
            iv_foot.setVisibility(Component.HIDE);
        }
        tv_title.setText(dataBean.getInfo().getTitle());
        return cpt;
    }
}
