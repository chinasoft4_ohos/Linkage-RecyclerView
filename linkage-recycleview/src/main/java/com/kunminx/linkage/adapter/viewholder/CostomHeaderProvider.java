/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kunminx.linkage.adapter.viewholder;

import com.kunminx.linkage.ResourceTable;
import com.kunminx.linkage.bean.ElemeGroupedItem;
import com.squareup.picasso.Picasso;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

import java.util.List;

public class CostomHeaderProvider extends BaseItemProvider {
    private List<ElemeGroupedItem.DataBean> list;
    private Component cpt;
    private Context context;
    private boolean isShow;

    /**
     * CostomHeaderProvider
     *
     * @param bean 实体类
     * @param context 上下文
     * @param isShow 是否显示
     */
    public CostomHeaderProvider(List<ElemeGroupedItem.DataBean> bean, Context context, boolean isShow) {
        this.list = bean;
        this.context = context;
        this.isShow = isShow;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        ElemeGroupedItem.DataBean dataBean = list.get(i);
        if (i == list.size() - 1) {
            cpt = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_default_adapter_linkage_secondary_footer, null, false);
        }else {
            if (dataBean.isIsHeader()) {
                cpt = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_default_adapter_linkage_secondary_header_two, null, false);
                Text tv_title = (Text) cpt.findComponentById(ResourceTable.Id_secondary_header);
                Button bt = (Button) cpt.findComponentById(ResourceTable.Id_bt_header_text);
                tv_title.setText(dataBean.getInfo().getGroup());
                bt.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        //new ToastDialog(context).setText("点击了 " + dataBean.getInfo().getGroup() + "-position" + i / 9).show();
                        initButtonClick("点击了 " + dataBean.getInfo().getGroup() + "  -  position  " + i / 9);
                    }
                });
            } else {
                cpt = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_default_adapter_linkage_secondary_linear, null, false);
                Text tv_title = (Text) cpt.findComponentById(ResourceTable.Id_level_2_title);
                Text tv_content = (Text) cpt.findComponentById(ResourceTable.Id_level_2_content);
                Image iv_foot = (Image) cpt.findComponentById(ResourceTable.Id_iv_foot);
                if (isShow) {
                    Picasso.get()
                            .load(list.get(i).getInfo().getImgUrl())
                            .placeholder(ResourceTable.Media_icon)
                            .error(ResourceTable.Media_icon)
                            .fit()
                            .tag(context)
                            .into(iv_foot);
                    tv_title.setTextColor(Color.BLACK);
                } else {
                    iv_foot.setVisibility(Component.HIDE);
                }
                tv_title.setText(dataBean.getInfo().getTitle());
                tv_content.setText(dataBean.getInfo().getContent());
            }
        }
        return cpt;
    }

    private void initButtonClick(String text) {
        ToastDialog toastDialog = new ToastDialog(context);
        toastDialog.setSize(ComponentContainer.LayoutConfig.MATCH_CONTENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
        Component parse = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_layout_toast_button, null, false);
        toastDialog.setComponent(parse);
        toastDialog.setTransparent(true);
        ((Text) parse.findComponentById(ResourceTable.Id_msg_toast)).setText(text);
        toastDialog.show();
    }
}
