/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kunminx.linkage.adapter;

import com.kunminx.linkage.ResourceTable;
import com.kunminx.linkage.bean.ElemeGroupedItem;
import com.squareup.picasso.Picasso;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.List;

public class LinkageSecondaryDialogAdapter extends BaseItemProvider {
    private List<ElemeGroupedItem.DataBean> list;
    private boolean isRemove;
    private Component cpt;
    private Context context;
    private boolean isShow;
    private onClickMyTextView onClickMyTextView;

    /**
     * CompanyProvider
     *
     * @param bean     实体类
     * @param isRemove 是否去除
     * @param context  上下文
     * @param isShow   是否展示
     */
    public LinkageSecondaryDialogAdapter(List<ElemeGroupedItem.DataBean> bean, boolean isRemove, Context context, boolean isShow) {
        this.list = bean;
        this.isRemove = isRemove;
        this.context = context;
        this.isShow = isShow;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    /**
     * initData
     */
    public void initData() {
        /*list.clear();
        if (list != null) {
            list.addAll(list);
        }*/
        notifyDataChanged();
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        ElemeGroupedItem.DataBean dataBean = list.get(i);
        if (i == list.size() - 1) {
            cpt = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_default_adapter_linkage_secondary_footer, null, false);
        } else {
            if (dataBean.isIsHeader()) {
                cpt = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_default_adapter_linkage_secondary_header, null, false);
                Text tv_title = (Text) cpt.findComponentById(ResourceTable.Id_secondary_header);
                tv_title.setText(dataBean.getInfo().getGroup());
                cpt.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {

                    }
                });
            } else {
                cpt = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_default_adapter_linkage_secondary_linear, null, false);
                Text tv_title = (Text) cpt.findComponentById(ResourceTable.Id_level_2_title);
                Text tv_content = (Text) cpt.findComponentById(ResourceTable.Id_level_2_content);
                Image iv_foot = (Image) cpt.findComponentById(ResourceTable.Id_iv_foot);
                StackLayout de_iamge = (StackLayout) cpt.findComponentById(ResourceTable.Id_de_iamge);
                if (isRemove) {
                    cpt.findComponentById(ResourceTable.Id_btn_food).setClickedListener(new Component.ClickedListener() {
                        @Override
                        public void onClick(Component component) {
                            list.remove(i);
                            notifyDataChanged();
                        }
                    });
                }
                DirectionalLayout dl_price = (DirectionalLayout) cpt.findComponentById(ResourceTable.Id_dl_price);
                if (isShow) {
                    Picasso.get()
                            .load(list.get(i).getInfo().getImgUrl())
                            .placeholder(ResourceTable.Media_icon)
                            .error(ResourceTable.Media_icon)
                            .fit()
                            .tag(context)
                            .into(iv_foot);
                    tv_title.setTextColor(Color.BLACK);
                    de_iamge.setVisibility(Component.VISIBLE);
                } else {
                    de_iamge.setVisibility(Component.HIDE);
                    iv_foot.setVisibility(Component.HIDE);
                    dl_price.setVisibility(Component.HIDE);
                    tv_content.setTextSize(40);

                }
                tv_title.setText(dataBean.getInfo().getTitle());
                tv_content.setText(dataBean.getInfo().getContent());
                cpt.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        onClickMyTextView.myTextViewClick();
                    }
                });
            }
        }
        return cpt;
    }

    //接口回调
    public interface onClickMyTextView{
        void myTextViewClick();
    }
    public void setOnClickMyTextView(onClickMyTextView onClickMyTextView) {
        this.onClickMyTextView = onClickMyTextView;
    }

}
