/*
 * Copyright (c) 2018-present. KunMinX
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kunminx.linkage.adapter;

import com.kunminx.linkage.ResourceTable;
import com.kunminx.linkage.bean.ElemeGroupedItem;
import com.kunminx.linkage.config.Api;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;

import java.util.List;

public class LinkagePrimaryAdapter extends RecycleItemProvider {
    private List<ElemeGroupedItem.DataBean> list;
    private AbilitySlice slice;
    private boolean isHide;
    private Api api;

    /**
     * TitleProvider
     *
     * @param stringList 集合
     * @param slice      上下文
     * @param isHide     是否隐藏
     * @param api        api
     */
    public LinkagePrimaryAdapter(List<ElemeGroupedItem.DataBean> stringList, AbilitySlice slice, boolean isHide, Api api) {
        this.list = stringList;
        this.slice = slice;
        this.isHide = isHide;
        this.api = api;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    /**
     * initData
     */
    public void initData() {
       /* list.clear();
        if (list != null) {
            list.addAll(list);
        }*/
        notifyDataChanged();
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        Component cpt = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_default_adapter_linkage_primary, null, false);
        ElemeGroupedItem.DataBean dataBean = list.get(i);
        Text tv_title = (Text) cpt.findComponentById(ResourceTable.Id_tv_group);
        DirectionalLayout dl_title_list = (DirectionalLayout) cpt.findComponentById(ResourceTable.Id_dl_title_list);
        tv_title.setText(dataBean.getInfo().getGroup());
        if (isHide) {
            if (i == list.size() - 1) {
                dl_title_list.setVisibility(Component.HIDE);
            }
        }
        if (i == 0) {
            api.setComponent(cpt);
            ShapeElement shapeElement2 = new ShapeElement();
            shapeElement2.setRgbColor(RgbColor.fromArgbInt(Color.rgb(111, 10, 238)));
            cpt.setBackground(shapeElement2);
            //dl_title_list.setBackground(shapeElement2);
            tv_title.setTextColor(Color.WHITE);
        }
        return cpt;
    }
}
