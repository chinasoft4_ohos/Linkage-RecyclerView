/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kunminx.linkage.config;

import ohos.agp.components.Component;

public class Api {
    public Api() {
        super();
    }

    /**
     * Component
     */
    private Component component;

    public Component getComponent() {
        return component;
    }

    public void setComponent(Component component) {
        this.component = component;
    }

    /**
     * getPositioninfo数据源
     *
     * @return String
     */
    public String getPositioninfo() {
        return "{\n" +
                "\t\"code\": \"1\",\n" +
                "\n" +
                "\t\"data\": [{\n" +
                "\t\t\t\"isHeader\": true,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"\",\n" +
                "\t\t\t\t\"group\": \"优惠\",\n" +
                "\t\t\t\t\"imgUrl\": \"\",\n" +
                "\t\t\t\t\"title\": \"\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"优惠\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"优惠\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png\",\n" +
                "\t\t\t\t\"title\": \"可乐\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"优惠\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-4a6889ba228feb46.png\",\n" +
                "\t\t\t\t\"title\": \"小食四拼\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"优惠\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-636381a9f7a855d6.png\",\n" +
                "\t\t\t\t\"title\": \"烤全翅\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"优惠\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-203db4255e78922a.png\",\n" +
                "\t\t\t\t\"title\": \"奥尔良鸡腿汉堡\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"优惠\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"鳕鱼堡\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"优惠\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png\",\n" +
                "\t\t\t\t\"title\": \"鸡腿\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"优惠\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-203db4255e78922a.png\",\n" +
                "\t\t\t\t\"title\": \"草莓派\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": true,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"\",\n" +
                "\t\t\t\t\"group\": \"套餐\",\n" +
                "\t\t\t\t\"imgUrl\": \"\",\n" +
                "\t\t\t\t\"title\": \"\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png\",\n" +
                "\t\t\t\t\"title\": \"可乐\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-4a6889ba228feb46.png\",\n" +
                "\t\t\t\t\"title\": \"小食四拼\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-636381a9f7a855d6.png\",\n" +
                "\t\t\t\t\"title\": \"烤全翅\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-203db4255e78922a.png\",\n" +
                "\t\t\t\t\"title\": \"奥尔良鸡腿汉堡\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"鳕鱼堡\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png\",\n" +
                "\t\t\t\t\"title\": \"鸡腿\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-203db4255e78922a.png\",\n" +
                "\t\t\t\t\"title\": \"草莓派\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": true,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"\",\n" +
                "\t\t\t\t\"group\": \"主食\",\n" +
                "\t\t\t\t\"imgUrl\": \"\",\n" +
                "\t\t\t\t\"title\": \"\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"主食\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"主食\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png\",\n" +
                "\t\t\t\t\"title\": \"可乐\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"主食\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-4a6889ba228feb46.png\",\n" +
                "\t\t\t\t\"title\": \"小食四拼\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"主食\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-636381a9f7a855d6.png\",\n" +
                "\t\t\t\t\"title\": \"烤全翅\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"主食\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-203db4255e78922a.png\",\n" +
                "\t\t\t\t\"title\": \"奥尔良鸡腿汉堡\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"主食\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"鳕鱼堡\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"主食\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png\",\n" +
                "\t\t\t\t\"title\": \"鸡腿\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"主食\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-203db4255e78922a.png\",\n" +
                "\t\t\t\t\"title\": \"草莓派\"\n" +
                "\t\t\t}\n" +
                "\t\t}, {\n" +
                "\t\t\t\"isHeader\": true,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"\",\n" +
                "\t\t\t\t\"group\": \"饮料\",\n" +
                "\t\t\t\t\"imgUrl\": \"\",\n" +
                "\t\t\t\t\"title\": \"\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"饮料\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"饮料\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png\",\n" +
                "\t\t\t\t\"title\": \"可乐\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"饮料\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-4a6889ba228feb46.png\",\n" +
                "\t\t\t\t\"title\": \"小食四拼\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"饮料\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-636381a9f7a855d6.png\",\n" +
                "\t\t\t\t\"title\": \"烤全翅\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"饮料\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-203db4255e78922a.png\",\n" +
                "\t\t\t\t\"title\": \"奥尔良鸡腿汉堡\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"饮料\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"鳕鱼堡\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"饮料\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png\",\n" +
                "\t\t\t\t\"title\": \"鸡腿\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"饮料\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-203db4255e78922a.png\",\n" +
                "\t\t\t\t\"title\": \"草莓派\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": true,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"\",\n" +
                "\t\t\t\t\"group\": \"套餐A\",\n" +
                "\t\t\t\t\"imgUrl\": \"\",\n" +
                "\t\t\t\t\"title\": \"\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐A\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐A\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png\",\n" +
                "\t\t\t\t\"title\": \"可乐\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐A\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-4a6889ba228feb46.png\",\n" +
                "\t\t\t\t\"title\": \"小食四拼\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐A\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-636381a9f7a855d6.png\",\n" +
                "\t\t\t\t\"title\": \"烤全翅\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐A\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-203db4255e78922a.png\",\n" +
                "\t\t\t\t\"title\": \"奥尔良鸡腿汉堡\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐A\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"鳕鱼堡\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐A\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png\",\n" +
                "\t\t\t\t\"title\": \"鸡腿\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐A\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-203db4255e78922a.png\",\n" +
                "\t\t\t\t\"title\": \"草莓派\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": true,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"\",\n" +
                "\t\t\t\t\"group\": \"套餐B\",\n" +
                "\t\t\t\t\"imgUrl\": \"\",\n" +
                "\t\t\t\t\"title\": \"\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐B\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐B\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png\",\n" +
                "\t\t\t\t\"title\": \"可乐\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐B\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-4a6889ba228feb46.png\",\n" +
                "\t\t\t\t\"title\": \"小食四拼\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐B\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-636381a9f7a855d6.png\",\n" +
                "\t\t\t\t\"title\": \"烤全翅\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐B\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-203db4255e78922a.png\",\n" +
                "\t\t\t\t\"title\": \"奥尔良鸡腿汉堡\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐B\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"鳕鱼堡\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐B\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png\",\n" +
                "\t\t\t\t\"title\": \"鸡腿\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐B\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-203db4255e78922a.png\",\n" +
                "\t\t\t\t\"title\": \"草莓派\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": true,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"\",\n" +
                "\t\t\t\t\"group\": \"套餐C\",\n" +
                "\t\t\t\t\"imgUrl\": \"\",\n" +
                "\t\t\t\t\"title\": \"\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐C\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐C\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png\",\n" +
                "\t\t\t\t\"title\": \"可乐\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐C\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-4a6889ba228feb46.png\",\n" +
                "\t\t\t\t\"title\": \"小食四拼\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐C\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-636381a9f7a855d6.png\",\n" +
                "\t\t\t\t\"title\": \"烤全翅\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐C\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-203db4255e78922a.png\",\n" +
                "\t\t\t\t\"title\": \"奥尔良鸡腿汉堡\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐C\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"鳕鱼堡\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐C\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png\",\n" +
                "\t\t\t\t\"title\": \"鸡腿\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"套餐C\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-203db4255e78922a.png\",\n" +
                "\t\t\t\t\"title\": \"草莓派\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": true,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\t\"group\": \"\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-203db4255e78922a.png\",\n" +
                "\t\t\t\t\"title\": \"草莓派\"\n" +
                "\t\t\t}\n" +
                "\t\t}\n" +
                "\t]\n" +
                "}";
    }

    /**
     * getCompanyInfo数据源
     *
     * @return String
     */
    public String getCompanyInfo() {
        return "{\n" +
                "\t\"code\": \"1\",\n" +
                "\n" +
                "\t\"data\": [{\n" +
                "\t\t\t\"isHeader\": true,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Complex data traversal\",\n" +
                "\t\t\t\t\"group\": \"Creating\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"just\"\n" +
                "\t\t\t}\n" +
                "\t\t}, {\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Complex data traversal\",\n" +
                "\t\t\t\t\"group\": \"Creating\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"just\"\n" +
                "\t\t\t}\n" +
                "\t\t}, {\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Complex data traversal\",\n" +
                "\t\t\t\t\"group\": \"Creating\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"fromArray\"\n" +
                "\t\t\t}\n" +
                "\t\t}, {\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Complex data traversal\",\n" +
                "\t\t\t\t\"group\": \"Creating\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"fromIterable\"\n" +
                "\t\t\t}\n" +
                "\t\t}, {\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Timed task\",\n" +
                "\t\t\t\t\"group\": \"Creating\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"interval\"\n" +
                "\t\t\t}\n" +
                "\t\t}, {\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Timed task\",\n" +
                "\t\t\t\t\"group\": \"Creating\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"intervalRange\"\n" +
                "\t\t\t}\n" +
                "\t\t}, {\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Nested callback asynchronous event\",\n" +
                "\t\t\t\t\"group\": \"Creating\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"create\"\n" +
                "\t\t\t}\n" +
                "\t\t}, {\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Delayed task\",\n" +
                "\t\t\t\t\"group\": \"Creating\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"defer\"\n" +
                "\t\t\t}\n" +
                "\t\t}, {\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Delayed task\",\n" +
                "\t\t\t\t\"group\": \"Creating\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"timer\"\n" +
                "\t\t\t}\n" +
                "\t\t}, {\n" +
                "\t\t\t\"isHeader\": true,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Transformation\",\n" +
                "\t\t\t\t\"group\": \"Transforming\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"map\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Transformation\",\n" +
                "\t\t\t\t\"group\": \"Transforming\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"map\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Transformation\",\n" +
                "\t\t\t\t\"group\": \"Transforming\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"flatMap\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Transformation\",\n" +
                "\t\t\t\t\"group\": \"Transforming\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"concatMap\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": true,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Combine multiple observers, and merge events\",\n" +
                "\t\t\t\t\"group\": \"Combining\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"concatArray\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Combine multiple observers, and merge events\",\n" +
                "\t\t\t\t\"group\": \"Combining\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"concatArray\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Combine multiple observers, and merge events\",\n" +
                "\t\t\t\t\"group\": \"Combining\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"concatDelayError\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Combine multiple observers, and merge events\",\n" +
                "\t\t\t\t\"group\": \"Combining\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"megerArray\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Combine multiple observers, and merge events\",\n" +
                "\t\t\t\t\"group\": \"Combining\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"megerArrayDelayError\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Combine multiple observers and merge them into one observer\",\n" +
                "\t\t\t\t\"group\": \"Combining\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"zip\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Combine multiple observers and merge them into one observer\",\n" +
                "\t\t\t\t\"group\": \"Combining\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"combineLatest\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Append other events before sending the event\",\n" +
                "\t\t\t\t\"group\": \"Combining\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"startWithArray\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Combine multiple events into one event\",\n" +
                "\t\t\t\t\"group\": \"Combining\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"reduce\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Combine multiple events into one event\",\n" +
                "\t\t\t\t\"group\": \"Combining\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"collect\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Summarize the number of events sent\",\n" +
                "\t\t\t\t\"group\": \"Combining\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"count\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": true,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Specify filter criteria to filter the required events or data\",\n" +
                "\t\t\t\t\"group\": \"Filtering\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"filter\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Specify filter criteria to filter the required events or data\",\n" +
                "\t\t\t\t\"group\": \"Filtering\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"filter\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Filter specified types of events or data\",\n" +
                "\t\t\t\t\"group\": \"Filtering\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"ofType\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Events or data with unsatisfied filter conditions\",\n" +
                "\t\t\t\t\"group\": \"Filtering\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"skip\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Filter duplicate events or data\",\n" +
                "\t\t\t\t\"group\": \"Filtering\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"distinct\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Filter duplicate events or data\",\n" +
                "\t\t\t\t\"group\": \"Filtering\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"distinctUntilChanged\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Filter events or data by time or amount\",\n" +
                "\t\t\t\t\"group\": \"Filtering\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"take\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Filter events at the specified location\",\n" +
                "\t\t\t\t\"group\": \"Filtering\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"elementAt\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Filter events by time period\",\n" +
                "\t\t\t\t\"group\": \"Filtering\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"throttleFirst\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Filter events by time period\",\n" +
                "\t\t\t\t\"group\": \"Filtering\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"throttleLast\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": true,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Determine if all events are met\",\n" +
                "\t\t\t\t\"group\": \"Conditional\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"all\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Determine if all events are met\",\n" +
                "\t\t\t\t\"group\": \"Conditional\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"all\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"When the sent event judgment condition is not satisfied, the subsequent event reception will be terminated.\",\n" +
                "\t\t\t\t\"group\": \"Conditional\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"takeWhile\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Receive subsequent events when the sent event judgment condition is not met\",\n" +
                "\t\t\t\t\"group\": \"Conditional\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"skipWhile\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Same as filter\",\n" +
                "\t\t\t\t\"group\": \"Conditional\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"takeUntil\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Receive events other than the judgment condition\",\n" +
                "\t\t\t\t\"group\": \"Conditional\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"skipUntil\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Determine if the events sent by the two observers are the same\",\n" +
                "\t\t\t\t\"group\": \"Conditional\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"sequenceEqual\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Determine whether the specified data contains the specified data.\",\n" +
                "\t\t\t\t\"group\": \"Conditional\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"contains\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Determine if the sent data is empty\",\n" +
                "\t\t\t\t\"group\": \"Conditional\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"isEmpty\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Multiple observers receive only the \\\"first observed observer who successfully sent data\\\"\",\n" +
                "\t\t\t\t\"group\": \"Conditional\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"amb\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": true,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Send and receive event listeners\",\n" +
                "\t\t\t\t\"group\": \"Blocking\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"doOnEach\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Send and receive event listeners\",\n" +
                "\t\t\t\t\"group\": \"Blocking\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"doOnEach\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Send and receive event listeners\",\n" +
                "\t\t\t\t\"group\": \"Blocking\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"doOnSubscribe\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Send and receive event listeners\",\n" +
                "\t\t\t\t\"group\": \"Blocking\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"doOnNext\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Send and receive event listeners\",\n" +
                "\t\t\t\t\"group\": \"Blocking\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"doAfterNext\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Send and receive event listeners\",\n" +
                "\t\t\t\t\"group\": \"Blocking\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"doOnComplete\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Send and receive event listeners\",\n" +
                "\t\t\t\t\"group\": \"Blocking\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"doOnError\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Error or exception handling: catch exceptions and feedback special results, normal termination\",\n" +
                "\t\t\t\t\"group\": \"Blocking\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"onErrorReturn\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Error or exception handling: catch exception and return a new event\",\n" +
                "\t\t\t\t\"group\": \"Blocking\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"onErrorResumeNext\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Error or exception handling: exception retry\",\n" +
                "\t\t\t\t\"group\": \"Blocking\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"retry\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Event retransmission\",\n" +
                "\t\t\t\t\"group\": \"Blocking\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"repeat\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Event retransmission\",\n" +
                "\t\t\t\t\"group\": \"Blocking\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"repeatWhen\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Event retransmission\",\n" +
                "\t\t\t\t\"group\": \"Blocking\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"repeatUntil\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Delay sending an observer's event\",\n" +
                "\t\t\t\t\"group\": \"Blocking\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"delay\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"isHeader\": false,\n" +
                "\t\t\t\"info\": {\n" +
                "\t\t\t\t\"content\": \"Send event timeout processing\",\n" +
                "\t\t\t\t\"group\": \"Blocking\",\n" +
                "\t\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\t\"title\": \"timeout\"\n" +
                "\t\t\t}\n" +
                "\t\t}\n" +
                "\t]\n" +
                "}";
    }

    /**
     * getLessPrimary数据源
     *
     * @return String
     */
    public String getLessPrimary() {
        return "{\n" +
                "\t\"code\": \"1\",\n" +
                "\n" +
                "\t\"data\": [{\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"优惠\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"优惠\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"套餐\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png\",\n" +
                "\t\t\t\"title\": \"可乐\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"套餐\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"主食\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"主食\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"主食\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png\",\n" +
                "\t\t\t\"title\": \"可乐\"\n" +
                "\t\t}\n" +
                "\t},{\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"主食\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png\",\n" +
                "\t\t\t\"title\": \"可乐\"\n" +
                "\t\t}\n" +
                "\t},{\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应\\n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"主食\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png\",\n" +
                "\t\t\t\"title\": \"可乐\"\n" +
                "\t\t}\n" +
                "\t}]\n" +
                "}";
    }

    /**
     * getMuchPrimary数据源
     *
     * @return String
     */
    public String getMuchPrimary() {
        return "{\n" +
                "\t\"code\": \"1\",\n" +
                "\t\"data\": [{\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"优惠\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"优惠\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"套餐\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"套餐\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"主食\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"主食\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"主食\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png\",\n" +
                "\t\t\t\"title\": \"可乐\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"主食\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-2c0645f9b1a588de.png\",\n" +
                "\t\t\t\"title\": \"可乐\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"a\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"a\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"b\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"b\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"c\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"c\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"d\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"d\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"e\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"e\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"f\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"f\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"g\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"g\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"h\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"h\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"i\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"i\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"j\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"j\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"k\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"k\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"l\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"l\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"m\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"m\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"n\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"n\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"o\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"o\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"p\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"p\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"q\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"q\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"r\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"r\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"s\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"s\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"t\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"t\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"u\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"u\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"v\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"v\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"w\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"w\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"x\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"x\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"y\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"y\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": true,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"z\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"z\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"isHeader\": false,\n" +
                "\t\t\"info\": {\n" +
                "\t\t\t\"content\": \"好吃的食物，增肥神器，有求必应n月售10008 好评率100%\",\n" +
                "\t\t\t\"group\": \"z\",\n" +
                "\t\t\t\"imgUrl\": \"https://upload-images.jianshu.io/upload_images/57036-7d50230a36c40a94.png\",\n" +
                "\t\t\t\"title\": \"全家桶\"\n" +
                "\t\t}\n" +
                "\t}]\n" +
                "}";
    }
}
