# Linkage-RecyclerView

#### 项目介绍
- 项目名称：Linkage-RecyclerView
- 所属系列：openharmony的第三方组件适配移植
- 功能：实现二级联动列表
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：master分支

#### 效果演示

|                           RxJava Magician                            |                         Eleme Linear                         |                          Eleme Grid                          |
| :----------------------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: |
| ![效果演示](./img/RxJava_Magician.gif) | ![效果演示](./img/Eleme_Linear.gif) | ![效果演示](./img/Eleme_Grid.gif) |


#### 安装教程

1、在项目根目录下的build.gradle文件中
 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
```
2、在entry模块的build.gradle文件中
 ```gradle
 dependencies {
    implementation('com.gitee.chinasoft_ohos:Linkage-RecyclerView:1.0.0')
    ......  
 }
```

在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明
 ```
使用该库非常简单，只需查看提供的示例的源代码。
<?xml version="1.0" encoding="utf-8"?>
<DirectionalLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:background_element="#fff"
    ohos:orientation="horizontal">

    <ListContainer
        ohos:id="$+id:rxmagic_header"
        ohos:height="match_parent"
        ohos:width="0"
        ohos:weight="1"/>

    <StackLayout
        ohos:weight="4"
        ohos:height="match_parent"
        ohos:orientation="vertical"
        ohos:width="0">

        <ListContainer
            ohos:id="$+id:rxmagic_context"
            ohos:height="match_parent"
            ohos:width="match_parent"/>

        <Text
            ohos:visibility="hide"
            ohos:id="$+id:title_heard"
            ohos:padding="20"
            ohos:text_size="50"
            ohos:background_element="#C0C0C0"
            ohos:height="match_content"
            ohos:width="match_parent"/>

    </StackLayout>

</DirectionalLayout>
```

```java
     ListContainer header = (ListContainer) findComponentById(ResourceTable.Id_eleme_header);
        ListContainer context = (ListContainer) findComponentById(ResourceTable.Id_eleme_context);
        Text heard = (Text) findComponentById(ResourceTable.Id_eleme_title_heard);
        initRecycleView(header, context, heard, Api.getPositioninfo(), true,false,true);
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原项目组件基本无差异


#### 版本迭代

- 1.0.0

#### 版权和许可信息
```

Copyright 2018-present KunMinX

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

```