package com.kunminx.linkagelistview;

import com.google.gson.Gson;
import com.kunminx.linkage.adapter.LinkagePrimaryAdapter;
import com.kunminx.linkage.adapter.LinkageSecondaryAdapter;
import com.kunminx.linkage.adapter.LinkageSecondaryDialogAdapter;
import com.kunminx.linkage.adapter.LinkageSecondaryTwoAdapter;
import com.kunminx.linkage.adapter.viewholder.BigProvider;
import com.kunminx.linkage.adapter.viewholder.CostomHeaderProvider;
import com.kunminx.linkage.adapter.viewholder.TitleTwoProvider;
import com.kunminx.linkage.bean.ElemeGroupedItem;
import com.kunminx.linkage.config.Api;
import com.kunminx.linkagelistview.adapter.MainPagerAdapter;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.ToastDialog;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.multimodalinput.event.TouchEvent;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainAbilitySlice extends AbilitySlice implements TabList.TabSelectedListener, PageSlider.PageChangedListener {
    private PageSlider pageSlider;
    private List<Component> pageviews;
    private TabList tabList;
    private LinkageSecondaryAdapter linkageSecondaryAdapter;
    private ShapeElement shapeElement1;
    private ShapeElement shapeElement2;
    private AnimatorValue animationIn;
    private AnimatorValue animationOut;
    private boolean isFlag = true;
    private boolean isFlag2 = true;
    private boolean isFlag3 = true;
    private DirectionalLayout directionalLayout;
    private Button buttonDissmiss;
    private DirectionalLayout dl_bottomsheet;
    private ShapeElement element2;
    private ToastDialog toastDialog;
    private Api api;

    private VelocityDetector mVelocityDetector = VelocityDetector.obtainInstance();
    private MyTouchEvent mTouchEvent = new MyTouchEvent();

    private float mLastX;
    private float mLastY;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initFindById();
        initData();
    }

    private void initFindById() {
        tabList = (TabList) findComponentById(ResourceTable.Id_tab_list);
        pageSlider = (PageSlider) findComponentById(ResourceTable.Id_pager_slider);

        api = new Api();
        tabList.addTabSelectedListener(this);
        pageSlider.addPageChangedListener(this);
        shapeElement1 = new ShapeElement();
        shapeElement1.setRgbColor(RgbColor.fromArgbInt(Color.WHITE.getValue()));
        shapeElement2 = new ShapeElement();
        shapeElement2.setRgbColor(RgbColor.fromArgbInt(Color.rgb(111, 10, 238)));
    }

    private void initData() {
        try {
            String[] tabsTitle = getResourceManager().getElement(ResourceTable.Strarray_tab_title).getStringArray();
            for (String title : tabsTitle) {
                TabList.Tab tab = tabList.new Tab(getContext());
                tab.setText(title);
                tab.setPadding(6, 0, 6, 0);
                tab.setMinWidth(130);
                tab.setMaxTextWidth(525);
                tabList.addTab(tab);
            }
            tabList.selectTabAt(0);
            pageSlider.setCurrentPage(0);
            pageSlider.setReboundEffect(false);
            pageSlider.setCentralScrollMode(true);
            pageviews = new ArrayList<>();
            LayoutScatter layoutScatter = LayoutScatter.getInstance(getContext());
            DirectionalLayout dependentLayout1 = (DirectionalLayout) layoutScatter.parse(ResourceTable.Layout_fragment_rxmagic, null, false);
            pageviews.add(dependentLayout1);
            DirectionalLayout dependentLayout2 = (DirectionalLayout) layoutScatter.parse(ResourceTable.Layout_fragment_eleme, null, false);
            pageviews.add(dependentLayout2);
            DirectionalLayout dependentLayout3 = (DirectionalLayout) layoutScatter.parse(ResourceTable.Layout_fragment_switch, null, false);
            pageviews.add(dependentLayout3);
            DirectionalLayout dependentLayout4 = (DirectionalLayout) layoutScatter.parse(ResourceTable.Layout_fragment_custom_header, null, false);
            pageviews.add(dependentLayout4);
            DirectionalLayout dependentLayout5 = (DirectionalLayout) layoutScatter.parse(ResourceTable.Layout_fragment_custom_decoration, null, false);
            pageviews.add(dependentLayout5);
            DirectionalLayout dependentLayout6 = (DirectionalLayout) layoutScatter.parse(ResourceTable.Layout_fragment_custom_dlalog, null, false);
            pageviews.add(dependentLayout6);
            DirectionalLayout dependentLayout7 = (DirectionalLayout) layoutScatter.parse(ResourceTable.Layout_fragment_bottomsheet, null, false);
            pageviews.add(dependentLayout7);
            DirectionalLayout dependentLayout8 = (DirectionalLayout) layoutScatter.parse(ResourceTable.Layout_fragment_much_primary_item, null, false);
            pageviews.add(dependentLayout8);
            DirectionalLayout dependentLayout9 = (DirectionalLayout) layoutScatter.parse(ResourceTable.Layout_fragment_less_primary_item, null, false);
            pageviews.add(dependentLayout9);
            pageSlider.setProvider(new MainPagerAdapter(pageviews));

            initRxMagic();
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
    }

    List<ElemeGroupedItem.DataBean> stringList = new ArrayList<>();

    /**
     * 首页
     */
    private void initRxMagic() {
        ListContainer ls_header = (ListContainer) findComponentById(ResourceTable.Id_rxmagic_header);
        ListContainer ls_context = (ListContainer) findComponentById(ResourceTable.Id_rxmagic_context);
        Text tv_heard = (Text) findComponentById(ResourceTable.Id_title_heard);
        initRxmagicLinkageData(ls_header, ls_context, tv_heard, api.getCompanyInfo(), false, false, false);
    }

    /**
     * Eleme
     */
    private void initEleme() {
        ListContainer header = (ListContainer) findComponentById(ResourceTable.Id_eleme_header);
        ListContainer context = (ListContainer) findComponentById(ResourceTable.Id_eleme_context);
        Text heard = (Text) findComponentById(ResourceTable.Id_eleme_title_heard);
        initLinkageData(header, context, heard, api.getPositioninfo(), true, false, true);
    }

    /**
     * Switch
     */
    private void initSwitch() {
        ListContainer header = (ListContainer) findComponentById(ResourceTable.Id_switch_header);
        ListContainer context = (ListContainer) findComponentById(ResourceTable.Id_switch_context);
        Text heard = (Text) findComponentById(ResourceTable.Id_switch_title_heard);
        Button bt_switch_bottom = (Button) findComponentById(ResourceTable.Id_bt_switch_bottom);

        initGrid(header, context, heard, 2, false);
        bt_switch_bottom.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (!isFlag3) {
                    initGrid(header, context, heard, 2, false);
                } else {
                    initGrid(header, context, heard, 1, false);
                }
                isFlag3 = !isFlag3;
                clickElement(bt_switch_bottom);
            }
        });
    }

    private void initCustomHeader() {
        Gson gson = new Gson();
        ElemeGroupedItem elemeGroupedItem = gson.fromJson(api.getPositioninfo(), ElemeGroupedItem.class);
        List<ElemeGroupedItem.DataBean> bean = elemeGroupedItem.getData();

        stringList.clear();

        for (ElemeGroupedItem.DataBean dataBean : bean) {
            if (dataBean.isIsHeader()) {
                stringList.add(dataBean);
            }
        }
        ListContainer ls_header = (ListContainer) findComponentById(ResourceTable.Id_custom_header_header);
        ListContainer ls_context = (ListContainer) findComponentById(ResourceTable.Id_custom_header_context);
        LinkagePrimaryAdapter linkagePrimaryAdapter = new LinkagePrimaryAdapter(stringList, MainAbilitySlice.this, true, api);
        ls_header.setItemProvider(linkagePrimaryAdapter);
        CostomHeaderProvider costomHeaderProvider = new CostomHeaderProvider(bean, this, true);
        ls_context.setItemProvider(costomHeaderProvider);
        Text tv_heard = (Text) findComponentById(ResourceTable.Id_custom_header_title_heard);
        Button bt_header_text = (Button) findComponentById(ResourceTable.Id_bt_header_text);

        DirectionalLayout directionalLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_dl_heard_title);

        ls_header.setItemClickedListener((listContainer, component, i, l) -> {
            String group = stringList.get(i).getInfo().getGroup();
            int index = bean.indexOf(stringList.get(i));
            leftListClickPotion = i;
            boolean isHeader = bean.get(index).isIsHeader();
            if (isHeader) {
                directionalLayout.setVisibility(Component.HIDE);
            } else {
                directionalLayout.setVisibility(Component.VISIBLE);
            }
            ls_context.scrollTo(index);
        });

        ls_context.setScrollListener(new ListContainer.ScrollListener() {
            @Override
            public void onScrollFinished() {
                if (ls_context.getFirstVisibleItemPosition() == 0) {
                    ls_header.getComponentAt(0).setBackground(shapeElement2);
                    Component componentAt1 = ls_header.getComponentAt(0);
                    ((Text) componentAt1.findComponentById(ResourceTable.Id_tv_group)).setTextColor(Color.WHITE);
                }
                leftListClickPotion = -1;
                int pos = ls_context.getItemPosByVisibleIndex(0);
                if (pos >= 0 && pos < bean.size()) {
                    for (int i4 = 0; i4 < stringList.size(); i4++) {
                        if (stringList.get(i4).getInfo().getGroup().equals(bean.get(pos).getInfo().getGroup())) {
                            ls_header.scrollTo(i4);
                            break;
                        }
                    }
                }
            }
        });

        ls_context.addScrolledListener((component, i, i1, i2, i3) -> {
            Component componentAt;
            int pos = ls_context.getItemPosByVisibleIndex(0);
            if (leftListClickPotion != -1) {
                ls_header.scrollTo(leftListClickPotion);
            }
            if (pos >= 0 && pos < bean.size()) {
                String group = bean.get(pos).getInfo().getGroup();
                for (int i4 = 0; i4 < stringList.size(); i4++) {
                    if (stringList.get(i4).getInfo().getGroup().equals(bean.get(pos).getInfo().getGroup())) {
                        if (api.getComponent() != null) {
                            if (leftListClickPotion == -1) {
                                ls_header.scrollTo(i4);
                            }
                            api.getComponent().setBackground(shapeElement1);
                            // 跑马灯效果
                            ((Text) api.getComponent().findComponentById(ResourceTable.Id_tv_group)).setTruncationMode(Text.TruncationMode.AUTO_SCROLLING);
                            // 始终处于自动滚动状态
                            ((Text) api.getComponent().findComponentById(ResourceTable.Id_tv_group)).setAutoScrollingCount(Text.AUTO_SCROLLING_FOREVER);
                            // 启动跑马灯效果
                            ((Text) api.getComponent().findComponentById(ResourceTable.Id_tv_group)).stopAutoScrolling();
                            // 设置text颜色
                            ((Text) api.getComponent().findComponentById(ResourceTable.Id_tv_group)).setTextColor(Color.GRAY);
                        }
                        if (leftListClickPotion == -1) {
                            componentAt = ls_header.getComponentAt(i4);
                        } else {
                            componentAt = ls_header.getComponentAt(leftListClickPotion);
                        }
                        componentAt.setBackground(shapeElement2);
                        ((Text) componentAt.findComponentById(ResourceTable.Id_tv_group)).startAutoScrolling();
                        ((Text) componentAt.findComponentById(ResourceTable.Id_tv_group)).setTextColor(Color.WHITE);
                        api.setComponent(componentAt);
                        break;
                    }
                }
                tv_heard.setText(group);
                directionalLayout.setVisibility(Component.HIDE);
                if (group.equals("")) {
                    tv_heard.setText("套餐C");
                    ls_header.getComponentAt(stringList.size() - 2).setBackground(shapeElement2);
                    Component componentAt1 = ls_header.getComponentAt(stringList.size() - 2);
                    ((Text) componentAt1.findComponentById(ResourceTable.Id_tv_group)).setTextColor(Color.WHITE);
                }
                if (pos > 0) {
                    directionalLayout.setVisibility(Component.VISIBLE);
                } else {
                    directionalLayout.setVisibility(Component.HIDE);
                }
                bt_header_text.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        if (pos / 9 == 7) {
                            initButtonClick("点击了 " + group + "  -  position  " + 6);
                        } else {
                            initButtonClick("点击了 " + group + "  -  position  " + pos / 9);
                        }

                    }
                });
            }
        });
    }

    private void initCustomDecoration() {
        ListContainer header = (ListContainer) findComponentById(ResourceTable.Id_custom_decoration_header);
        ListContainer context = (ListContainer) findComponentById(ResourceTable.Id_custom_decoration_context);
        Text heard = (Text) findComponentById(ResourceTable.Id_custom_decoration_title_heard);
        initGrid(header, context, heard, 2, true);
    }

    private void initCustomDlaLog() {
        ShapeElement element = new ShapeElement();
        ShapeElement element2 = new ShapeElement();
        element.setRgbColor(new RgbColor(0, 0, 0, 60));
        element2.setRgbColor(new RgbColor(255, 255, 255));
        ListContainer ls_header = (ListContainer) findComponentById(ResourceTable.Id_custom_dlalog_header);
        ListContainer ls_context = (ListContainer) findComponentById(ResourceTable.Id_custom_dlalog_context);
        Text tv_heard = (Text) findComponentById(ResourceTable.Id_custom_dlalog_title_heard);
        Text tv_toast = (Text) findComponentById(ResourceTable.Id_tv_toast);

        Button button = (Button) findComponentById(ResourceTable.Id_bt_custom_dlalog_bottom);

        DirectionalLayout directionalLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_dl_custom_dlalog_bottom);
        DependentLayout dl_bottomsheet = (DependentLayout) findComponentById(ResourceTable.Id_dl_custom_dlalog_bottomsheet);

        Gson gson = new Gson();
        ElemeGroupedItem elemeGroupedItem = gson.fromJson(api.getCompanyInfo(), ElemeGroupedItem.class);
        List<ElemeGroupedItem.DataBean> bean = elemeGroupedItem.getData();

        stringList.clear();

        for (ElemeGroupedItem.DataBean dataBean : bean) {
            if (dataBean.isIsHeader()) {
                stringList.add(dataBean);
            }
        }
        TitleTwoProvider titleProvider = new TitleTwoProvider(stringList, MainAbilitySlice.this, false);
        ls_header.setItemProvider(titleProvider);
        LinkageSecondaryDialogAdapter linkageSecondaryAdapter = new LinkageSecondaryDialogAdapter(bean, false, this, false);
        ls_context.setItemProvider(linkageSecondaryAdapter);

        ls_context.scrollTo(0);
        ls_header.setItemClickedListener((listContainer, component, i, l) -> {
            linkageSecondaryAdapter.initData();
            int index = bean.indexOf(stringList.get(i));
            boolean isHeader = bean.get(index).isIsHeader();
            if (isHeader) {
                tv_heard.setVisibility(Component.HIDE);
            } else {
                tv_heard.setVisibility(Component.VISIBLE);
            }
            ls_context.scrollTo(index);

            getUITaskDispatcher().delayDispatch(() -> {
                tv_toast.setVisibility(Component.HIDE);
                tv_toast.setText(stringList.get(i).getInfo().getGroup());
            }, 300);
            tv_toast.setVisibility(Component.VISIBLE);
            tv_toast.setText(stringList.get(i).getInfo().getGroup());

        });

        ls_context.setScrollListener(new ListContainer.ScrollListener() {
            @Override
            public void onScrollFinished() {
                if (ls_context.getFirstVisibleItemPosition() == 0) {
                    ls_header.getComponentAt(0).setBackground(shapeElement2);
                    Component componentAt1 = ls_header.getComponentAt(0);
                    ((Text) componentAt1.findComponentById(ResourceTable.Id_tv_group)).setTextColor(Color.WHITE);
                }
                int pos = ls_context.getItemPosByVisibleIndex(0);
                if (pos >= 0 && pos < bean.size()) {
                    for (int i4 = 0; i4 < stringList.size(); i4++) {
                        if (stringList.get(i4).getInfo().getGroup().equals(bean.get(pos).getInfo().getGroup())) {
                            ls_header.scrollTo(i4);
                            break;
                        }
                    }
                }
            }
        });

        ls_context.addScrolledListener((component, i, i1, i2, i3) -> {
            Component componentAt;
            int pos = ls_context.getItemPosByVisibleIndex(0);
            ls_header.scrollTo(0);
            if (pos >= 0 && pos < bean.size()) {
                String group = bean.get(pos).getInfo().getGroup();
                for (int i4 = 0; i4 < stringList.size(); i4++) {
                    if (stringList.get(i4).getInfo().getGroup().equals(bean.get(pos).getInfo().getGroup())) {
                        if (api.getComponent() != null) {
                            ls_header.scrollTo(i4);
                            api.getComponent().setBackground(shapeElement1);
                            // 跑马灯效果
                            ((Text) api.getComponent().findComponentById(ResourceTable.Id_tv_group)).setTruncationMode(Text.TruncationMode.AUTO_SCROLLING);
                            // 始终处于自动滚动状态
                            ((Text) api.getComponent().findComponentById(ResourceTable.Id_tv_group)).setAutoScrollingCount(Text.AUTO_SCROLLING_FOREVER);
                            // 启动跑马灯效果
                            ((Text) api.getComponent().findComponentById(ResourceTable.Id_tv_group)).stopAutoScrolling();
                            // 设置text颜色
                            ((Text) api.getComponent().findComponentById(ResourceTable.Id_tv_group)).setTextColor(Color.GRAY);
                        }
                        componentAt = ls_header.getComponentAt(i4);
                        componentAt.setBackground(shapeElement2);
                        ((Text) componentAt.findComponentById(ResourceTable.Id_tv_group)).startAutoScrolling();
                        ((Text) componentAt.findComponentById(ResourceTable.Id_tv_group)).setTextColor(Color.WHITE);
                        api.setComponent(componentAt);
                        break;
                    }
                }
                tv_heard.setText(group);
                if (pos > 0) {
                    tv_heard.setVisibility(Component.VISIBLE);
                } else {
                    tv_heard.setVisibility(Component.HIDE);
                }
            }
        });

        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                ls_context.scrollTo(0);
                if (!isFlag2) {
                    dl_bottomsheet.setBackground(element2);
                    directionalLayout.setVisibility(Component.HIDE);
                } else {
                    dl_bottomsheet.setBackground(element);
                    directionalLayout.setVisibility(Component.VISIBLE);
                }
                isFlag2 = !isFlag2;
                clickElement(button);
            }
        });
        dl_bottomsheet.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                directionalLayout.setVisibility(Component.HIDE);
                button.setVisibility(Component.VISIBLE);
                dl_bottomsheet.setBackground(element2);
                ls_context.scrollTo(0);
                isFlag2 = !isFlag2;
            }
        });
        linkageSecondaryAdapter.setOnClickMyTextView(new LinkageSecondaryDialogAdapter.onClickMyTextView() {
            @Override
            public void myTextViewClick() {
                directionalLayout.setVisibility(Component.HIDE);
                button.setVisibility(Component.VISIBLE);
                dl_bottomsheet.setBackground(element2);
                ls_context.scrollTo(0);
                isFlag2 = !isFlag2;
            }
        });
    }

    private void initBottomSheet() {
        ShapeElement element = new ShapeElement();
        element2 = new ShapeElement();
        element.setRgbColor(new RgbColor(0, 0, 0, 60));
        element2.setRgbColor(new RgbColor(255, 255, 255));
        ListContainer header = (ListContainer) findComponentById(ResourceTable.Id_bottomsheet_header);
        ListContainer context = (ListContainer) findComponentById(ResourceTable.Id_bottomsheet_context);
        Text heard = (Text) findComponentById(ResourceTable.Id_bottomsheet_title_heard);
        initRecycle(header, context, heard, api.getCompanyInfo(), false, false, false);

        buttonDissmiss = (Button) findComponentById(ResourceTable.Id_bt_bottom);

        directionalLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_dl_bottom);
        dl_bottomsheet = (DirectionalLayout) findComponentById(ResourceTable.Id_dl_bottomsheet);
        buttonDissmiss.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (!isFlag) {
                    dl_bottomsheet.setBackground(element2);
                    dissMiss();
                } else {
                    dl_bottomsheet.setBackground(element);
                    buttonDissmiss.setVisibility(Component.HIDE);
                    show();
                    context.scrollTo(0);
                }
            }
        });
        dl_bottomsheet.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                dl_bottomsheet.setBackground(element2);
                if (directionalLayout.getVisibility() != Component.HIDE) {
                    dissMiss();
                    context.scrollTo(0);
                }
            }
        });
    }

    private void initMuchPrimary() {
        ListContainer header = (ListContainer) findComponentById(ResourceTable.Id_much_primary_header);
        ListContainer context = (ListContainer) findComponentById(ResourceTable.Id_much_primary_context);
        Text heard = (Text) findComponentById(ResourceTable.Id_much_primary_title_heard);
        initRecycleData(header, context, heard, api.getMuchPrimary(), true, false, false);
    }

    private void initLessPrimary() {
        ListContainer header = (ListContainer) findComponentById(ResourceTable.Id_less_primary_header);
        ListContainer context = (ListContainer) findComponentById(ResourceTable.Id_less_primary_context);
        Text heard = (Text) findComponentById(ResourceTable.Id_less_primary_title_heard);
        initLinkageData(header, context, heard, api.getLessPrimary(), true, true, false);
    }

    private int leftListClickPotion = -1;

    // 封装网格方法
    private void initGrid(ListContainer header, ListContainer context, Text heard, int Number, boolean isColor) {
        Gson gson = new Gson();
        ElemeGroupedItem elemeGroupedItem = gson.fromJson(api.getPositioninfo(), ElemeGroupedItem.class);
        List<ElemeGroupedItem.DataBean> bean = elemeGroupedItem.getData();

        stringList.clear();

        for (ElemeGroupedItem.DataBean dataBean : bean) {
            if (dataBean.isIsHeader()) {
                stringList.add(dataBean);
            }
        }

        LinkagePrimaryAdapter linkagePrimaryAdapter = new LinkagePrimaryAdapter(stringList, this, true, api);
        header.setItemProvider(linkagePrimaryAdapter);

        BigProvider bigProvider = new BigProvider(bean, stringList, this, Number, isColor);
        context.setItemProvider(bigProvider);

        context.scrollTo(0);

        header.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int i, long l) {
                int index = bean.indexOf(stringList.get(i));
                boolean isHeader = bean.get(index).isIsHeader();
                if (isHeader) {
                    heard.setVisibility(Component.HIDE);
                } else {
                    heard.setVisibility(Component.VISIBLE);
                }
                leftListClickPotion = i;
                context.scrollTo(i);
            }
        });

        context.setScrollListener(() -> {
            int position = context.getFirstVisibleItemPosition();
            try {
                String group = stringList.get(position).getInfo().getGroup();
                header.scrollTo(position);
                if (position == 0) {
                    header.getComponentAt(position).setBackground(shapeElement2);
                    Component componentAt1 = header.getComponentAt(0);
                    ((Text) componentAt1.findComponentById(ResourceTable.Id_tv_group)).setTextColor(Color.WHITE);
                    heard.setVisibility(Component.VISIBLE);
                    heard.setText(group);
                }
            } catch (Exception e) {
                e.getMessage();
            }
        });

        context.addScrolledListener((component, i, i1, i2, i3) -> {
            int position = context.getFirstVisibleItemPosition();
            try {
                String group = stringList.get(position).getInfo().getGroup();
                if (api.getComponent() != null) {
                    api.getComponent().setBackground(shapeElement1);
                    //设置text颜色
                    ((Text) api.getComponent().findComponentById(ResourceTable.Id_tv_group)).setTextColor(Color.GRAY);
                }
                api.setComponent(header.getComponentAt(position));
                header.getComponentAt(position).setBackground(shapeElement2);
                ((Text) api.getComponent().findComponentById(ResourceTable.Id_tv_group)).setTextColor(Color.WHITE);
                heard.setVisibility(Component.VISIBLE);
                heard.setText(group);
                if (group.equals("")) {
                    heard.setText("套餐C");
                    header.getComponentAt(stringList.size() - 2).setBackground(shapeElement2);
                    api.setComponent(header.getComponentAt(stringList.size() - 2));
                    ((Text) api.getComponent().findComponentById(ResourceTable.Id_tv_group)).setTextColor(Color.WHITE);
                }
                header.scrollTo(position);
            } catch (Exception e) {
                e.getMessage();
            }
        });
    }

    // 封装适配器方法
    private void initRecycleData(ListContainer ls_header, ListContainer ls_context, Text tv_heard, String json, boolean isShow, boolean isRemove, boolean isHide) {
        Gson gson = new Gson();
        ElemeGroupedItem elemeGroupedItem = gson.fromJson(json, ElemeGroupedItem.class);
        List<ElemeGroupedItem.DataBean> bean = elemeGroupedItem.getData();

        stringList.clear();

        for (ElemeGroupedItem.DataBean dataBean : bean) {
            if (dataBean.isIsHeader()) {
                stringList.add(dataBean);
            }
        }
        LinkagePrimaryAdapter linkagePrimaryAdapter = new LinkagePrimaryAdapter(stringList, MainAbilitySlice.this, isHide, api);
        ls_header.setItemProvider(linkagePrimaryAdapter);
        linkageSecondaryAdapter = new LinkageSecondaryAdapter(bean, isRemove, this, isShow);
        ls_context.setItemProvider(linkageSecondaryAdapter);

        // ls_context.scrollTo(0);
        ls_header.setItemClickedListener((listContainer, component, i, l) -> {
            linkageSecondaryAdapter.initData();
            int index = bean.indexOf(stringList.get(i));
            leftListClickPotion = i;
            boolean isHeader = bean.get(index).isIsHeader();
            if (isHeader) {
                tv_heard.setVisibility(Component.HIDE);
            } else {
                tv_heard.setVisibility(Component.VISIBLE);
            }
            ls_context.scrollTo(index);
            if (!isShow) {
                toastDialog = new ToastDialog(getContext());
                toastDialog.setSize(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
                Component parse = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_layout_toast, null, false);
                toastDialog.setComponent(parse);
                ((Text) parse.findComponentById(ResourceTable.Id_msg_toast)).setText(stringList.get(i).getInfo().getGroup());
                toastDialog.show();
                tv_heard.setText(stringList.get(i).getInfo().getGroup());
            }
        });

        ls_context.setScrollListener(new ListContainer.ScrollListener() {
            @Override
            public void onScrollFinished() {
                leftListClickPotion = -1;
                int pos = ls_context.getItemPosByVisibleIndex(0);
                if (pos >= 0 && pos < bean.size()) {
                    for (ElemeGroupedItem.DataBean dataBean : stringList) {
                        if (dataBean.getInfo().getGroup().equals(bean.get(pos).getInfo().getGroup())) {
                            break;
                        }
                    }
                }
            }
        });

        ls_context.addScrolledListener((component, i, i1, i2, i3) -> {
            Component componentAt;
            int pos = ls_context.getItemPosByVisibleIndex(0);

            if (pos >= 0 && pos < bean.size()) {
                String group = bean.get(pos).getInfo().getGroup();
                for (int i4 = 0; i4 < stringList.size(); i4++) {
                    if (stringList.get(i4).getInfo().getGroup().equals(bean.get(pos).getInfo().getGroup())) {
                        if (api.getComponent() != null) {
                            api.getComponent().setBackground(shapeElement1);
                            // 跑马灯效果
                            ((Text) api.getComponent().findComponentById(ResourceTable.Id_tv_group)).setTruncationMode(Text.TruncationMode.AUTO_SCROLLING);
                            // 始终处于自动滚动状态
                            ((Text) api.getComponent().findComponentById(ResourceTable.Id_tv_group)).setAutoScrollingCount(Text.AUTO_SCROLLING_FOREVER);
                            // 启动跑马灯效果
                            ((Text) api.getComponent().findComponentById(ResourceTable.Id_tv_group)).stopAutoScrolling();
                            // 设置text颜色
                            ((Text) api.getComponent().findComponentById(ResourceTable.Id_tv_group)).setTextColor(Color.GRAY);
                        }
                        if (leftListClickPotion == -1) {
                            componentAt = ls_header.getComponentAt(i4);
                        } else {
                            componentAt = ls_header.getComponentAt(leftListClickPotion);
                        }
                        if (componentAt != null) {
                            componentAt.setBackground(shapeElement2);
                            ((Text) componentAt.findComponentById(ResourceTable.Id_tv_group)).startAutoScrolling();
                            ((Text) componentAt.findComponentById(ResourceTable.Id_tv_group)).setTextColor(Color.WHITE);
                            api.setComponent(componentAt);
                        }
                        break;
                    }
                }
                tv_heard.setText(group);
                tv_heard.setVisibility(Component.HIDE);
                if (pos > 0) {
                    tv_heard.setVisibility(Component.VISIBLE);
                } else {
                    tv_heard.setVisibility(Component.HIDE);
                }
            }
        });
    }

    private void initLinkageData(ListContainer ls_header, ListContainer ls_context, Text tv_heard, String json, boolean isShow, boolean isRemove, boolean isHide) {
        Gson gson = new Gson();
        ElemeGroupedItem elemeGroupedItem = gson.fromJson(json, ElemeGroupedItem.class);
        List<ElemeGroupedItem.DataBean> bean = elemeGroupedItem.getData();

        stringList.clear();

        for (ElemeGroupedItem.DataBean dataBean : bean) {
            if (dataBean.isIsHeader()) {
                stringList.add(dataBean);
            }
        }
        LinkagePrimaryAdapter linkagePrimaryAdapter = new LinkagePrimaryAdapter(stringList, MainAbilitySlice.this, isHide, api);
        ls_header.setItemProvider(linkagePrimaryAdapter);
        linkageSecondaryAdapter = new LinkageSecondaryAdapter(bean, isRemove, this, isShow);
        ls_context.setItemProvider(linkageSecondaryAdapter);
        ls_context.scrollTo(0);
        ls_header.setItemClickedListener((listContainer, component, i, l) -> {
            linkageSecondaryAdapter.initData();
            int index = bean.indexOf(stringList.get(i));
            leftListClickPotion = i;
            boolean isHeader = bean.get(index).isIsHeader();
            if (isHeader) {
                tv_heard.setVisibility(Component.HIDE);
            } else {
                tv_heard.setVisibility(Component.VISIBLE);
            }
            ls_context.scrollTo(index);
            if (isShow == false) {
                ToastDialog toastDialog = new ToastDialog(getContext());
                toastDialog.setSize(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
                Component parse = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_layout_toast, null, false);
                toastDialog.setComponent(parse);
                ((Text) parse.findComponentById(ResourceTable.Id_msg_toast)).setText(stringList.get(i).getInfo().getGroup());
                toastDialog.show();
                tv_heard.setText(stringList.get(i).getInfo().getGroup());
            }
        });

        ls_context.setScrollListener(new ListContainer.ScrollListener() {
            @Override
            public void onScrollFinished() {
                if (ls_context.getFirstVisibleItemPosition() == 0) {
                    ls_header.getComponentAt(0).setBackground(shapeElement2);
                    Component componentAt1 = ls_header.getComponentAt(0);
                    ((Text) componentAt1.findComponentById(ResourceTable.Id_tv_group)).setTextColor(Color.WHITE);
                }
                leftListClickPotion = -1;
                int pos = ls_context.getItemPosByVisibleIndex(0);
                if (pos >= 0 && pos < bean.size()) {
                    for (int i4 = 0; i4 < stringList.size(); i4++) {
                        if (stringList.get(i4).getInfo().getGroup().equals(bean.get(pos).getInfo().getGroup())) {
                            ls_header.scrollTo(i4);
                            break;
                        }
                    }
                }
            }
        });

        ls_context.addScrolledListener((component, i, i1, i2, i3) -> {
            Component componentAt;
            int pos = ls_context.getItemPosByVisibleIndex(0);
            if (leftListClickPotion != -1) {
                ls_header.scrollTo(leftListClickPotion);
            }
            if (pos >= 0 && pos < bean.size()) {
                String group = bean.get(pos).getInfo().getGroup();
                for (int i4 = 0; i4 < stringList.size(); i4++) {
                    if (stringList.get(i4).getInfo().getGroup().equals(bean.get(pos).getInfo().getGroup())) {
                        if (api.getComponent() != null) {
                            if (leftListClickPotion == -1) {
                                ls_header.scrollTo(i4);
                            }
                            api.getComponent().setBackground(shapeElement1);
                            // 跑马灯效果
                            ((Text) api.getComponent().findComponentById(ResourceTable.Id_tv_group)).setTruncationMode(Text.TruncationMode.AUTO_SCROLLING);
                            // 始终处于自动滚动状态
                            ((Text) api.getComponent().findComponentById(ResourceTable.Id_tv_group)).setAutoScrollingCount(Text.AUTO_SCROLLING_FOREVER);
                            // 启动跑马灯效果
                            ((Text) api.getComponent().findComponentById(ResourceTable.Id_tv_group)).stopAutoScrolling();
                            // 设置text颜色
                            ((Text) api.getComponent().findComponentById(ResourceTable.Id_tv_group)).setTextColor(Color.GRAY);
                        }

                        if (leftListClickPotion == -1) {
                            componentAt = ls_header.getComponentAt(i4);
                        } else {
                            componentAt = ls_header.getComponentAt(i4);
                        }
                        componentAt.setBackground(shapeElement2);
                        ((Text) componentAt.findComponentById(ResourceTable.Id_tv_group)).startAutoScrolling();
                        ((Text) componentAt.findComponentById(ResourceTable.Id_tv_group)).setTextColor(Color.WHITE);
                        api.setComponent(componentAt);
                        break;
                    }
                }
                tv_heard.setText(group);
                if (group.equals("")) {
                    tv_heard.setText("套餐C");
                    ls_header.getComponentAt(stringList.size() - 2).setBackground(shapeElement2);
                    api.setComponent(ls_header.getComponentAt(stringList.size() - 2));
                    ((Text) api.getComponent().findComponentById(ResourceTable.Id_tv_group)).setTextColor(Color.WHITE);
                }
                tv_heard.setVisibility(Component.HIDE);
                if (pos > 0) {
                    tv_heard.setVisibility(Component.VISIBLE);
                } else {
                    tv_heard.setVisibility(Component.HIDE);
                }
            }
        });
    }

    //首页
    private void initRxmagicLinkageData(ListContainer ls_header, ListContainer ls_context, Text tv_heard, String json, boolean isShow, boolean isRemove, boolean isHide) {
        Gson gson = new Gson();
        ElemeGroupedItem elemeGroupedItem = gson.fromJson(json, ElemeGroupedItem.class);
        List<ElemeGroupedItem.DataBean> bean = elemeGroupedItem.getData();

        stringList.clear();

        for (ElemeGroupedItem.DataBean dataBean : bean) {
            if (dataBean.isIsHeader()) {
                stringList.add(dataBean);
            }
        }
        LinkagePrimaryAdapter linkagePrimaryAdapter = new LinkagePrimaryAdapter(stringList, MainAbilitySlice.this, isHide, api);
        ls_header.setItemProvider(linkagePrimaryAdapter);
        LinkageSecondaryTwoAdapter linkageSecondaryTwoAdapter = new LinkageSecondaryTwoAdapter(bean, isRemove, this, isShow);
        ls_context.setItemProvider(linkageSecondaryTwoAdapter);
        ls_context.scrollTo(0);
        ls_header.setItemClickedListener((listContainer, component, i, l) -> {
            linkageSecondaryTwoAdapter.initData();
            int index = bean.indexOf(stringList.get(i));
            leftListClickPotion = i;
            boolean isHeader = bean.get(index).isIsHeader();
            if (isHeader) {
                tv_heard.setVisibility(Component.HIDE);
            } else {
                tv_heard.setVisibility(Component.VISIBLE);
            }
            ls_context.scrollTo(index);
            if (!isShow) {
                toastDialog = new ToastDialog(getContext());
                toastDialog.setSize(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
                Component parse = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_layout_toast, null, false);
                toastDialog.setComponent(parse);
                ((Text) parse.findComponentById(ResourceTable.Id_msg_toast)).setText(stringList.get(i).getInfo().getGroup());
                toastDialog.show();
                tv_heard.setText(stringList.get(i).getInfo().getGroup());
            }
        });

        ls_context.setScrollListener(new ListContainer.ScrollListener() {
            @Override
            public void onScrollFinished() {
                if (ls_context.getFirstVisibleItemPosition() == 0) {
                    ls_header.getComponentAt(0).setBackground(shapeElement2);
                    Component componentAt1 = ls_header.getComponentAt(0);
                    ((Text) componentAt1.findComponentById(ResourceTable.Id_tv_group)).setTextColor(Color.WHITE);
                }
                leftListClickPotion = -1;
                int pos = ls_context.getItemPosByVisibleIndex(0);
                if (pos >= 0 && pos < bean.size()) {
                    for (int i4 = 0; i4 < stringList.size(); i4++) {
                        if (stringList.get(i4).getInfo().getGroup().equals(bean.get(pos).getInfo().getGroup())) {
                            ls_header.scrollTo(i4);
                            break;
                        }
                    }
                }
            }
        });

        ls_context.addScrolledListener((component, i, i1, i2, i3) -> {
            Component componentAt;
            int pos = ls_context.getItemPosByVisibleIndex(0);
            if (leftListClickPotion != -1) {
                ls_header.scrollTo(leftListClickPotion);
            }
            if (pos >= 0 && pos < bean.size()) {
                String group = bean.get(pos).getInfo().getGroup();
                for (int i4 = 0; i4 < stringList.size(); i4++) {
                    if (stringList.get(i4).getInfo().getGroup().equals(bean.get(pos).getInfo().getGroup())) {
                        if (api.getComponent() != null) {
                            if (leftListClickPotion == -1) {
                                ls_header.scrollTo(i4);
                            }
                            api.getComponent().setBackground(shapeElement1);
                            // 跑马灯效果
                            ((Text) api.getComponent().findComponentById(ResourceTable.Id_tv_group)).setTruncationMode(Text.TruncationMode.AUTO_SCROLLING);
                            // 始终处于自动滚动状态
                            ((Text) api.getComponent().findComponentById(ResourceTable.Id_tv_group)).setAutoScrollingCount(Text.AUTO_SCROLLING_FOREVER);
                            // 启动跑马灯效果
                            ((Text) api.getComponent().findComponentById(ResourceTable.Id_tv_group)).stopAutoScrolling();
                            // 设置text颜色
                            ((Text) api.getComponent().findComponentById(ResourceTable.Id_tv_group)).setTextColor(Color.GRAY);
                        }

                        if (leftListClickPotion == -1) {
                            componentAt = ls_header.getComponentAt(i4);
                        } else {
                            componentAt = ls_header.getComponentAt(i4);
                        }
                        componentAt.setBackground(shapeElement2);
                        ((Text) componentAt.findComponentById(ResourceTable.Id_tv_group)).startAutoScrolling();
                        ((Text) componentAt.findComponentById(ResourceTable.Id_tv_group)).setTextColor(Color.WHITE);
                        api.setComponent(componentAt);
                        break;
                    }
                }
                tv_heard.setText(group);
                tv_heard.setVisibility(Component.HIDE);
                if (pos > 0) {
                    tv_heard.setVisibility(Component.VISIBLE);
                } else {
                    tv_heard.setVisibility(Component.HIDE);
                }
            }
        });
    }

    private void initRecycle(ListContainer ls_header, ListContainer ls_context, Text tv_heard, String json, boolean isShow, boolean isRemove, boolean isHide) {
        Gson gson = new Gson();
        ElemeGroupedItem elemeGroupedItem = gson.fromJson(json, ElemeGroupedItem.class);
        List<ElemeGroupedItem.DataBean> bean = elemeGroupedItem.getData();

        stringList.clear();

        for (ElemeGroupedItem.DataBean dataBean : bean) {
            if (dataBean.isIsHeader()) {
                stringList.add(dataBean);
            }
        }
        TitleTwoProvider titleProvider = new TitleTwoProvider(stringList, MainAbilitySlice.this, isHide);
        ls_header.setItemProvider(titleProvider);
        LinkageSecondaryDialogAdapter linkageSecondaryAdapter = new LinkageSecondaryDialogAdapter(bean, isRemove, this, isShow);
        ls_context.setItemProvider(linkageSecondaryAdapter);

        ls_header.setItemClickedListener((listContainer, component, i, l) -> {
            linkageSecondaryAdapter.initData();
            int index = bean.indexOf(stringList.get(i));

            tv_heard.setVisibility(Component.VISIBLE);
            ls_context.scrollTo(index);
            if (!isShow) {
                toastDialog = new ToastDialog(getContext());
                toastDialog.setSize(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
                Component parse = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_layout_toast, null, false);
                toastDialog.setComponent(parse);
                ((Text) parse.findComponentById(ResourceTable.Id_msg_toast)).setText(stringList.get(i).getInfo().getGroup());
                toastDialog.show();
                tv_heard.setText(stringList.get(i).getInfo().getGroup());
            }
        });

        ls_context.setScrollListener(new ListContainer.ScrollListener() {
            @Override
            public void onScrollFinished() {
                if (ls_context.getFirstVisibleItemPosition() == 0) {
                    ls_header.getComponentAt(0).setBackground(shapeElement2);
                    Component componentAt1 = ls_header.getComponentAt(0);
                    ((Text) componentAt1.findComponentById(ResourceTable.Id_tv_group)).setTextColor(Color.WHITE);
                }
                int pos = ls_context.getItemPosByVisibleIndex(0);
                if (pos >= 0 && pos < bean.size()) {
                    for (int i4 = 0; i4 < stringList.size(); i4++) {
                        if (stringList.get(i4).getInfo().getGroup().equals(bean.get(pos).getInfo().getGroup())) {
                            ls_header.scrollTo(i4);
                            break;
                        }
                    }
                }
            }
        });

        ls_context.addScrolledListener((component, i, i1, i2, i3) -> {
            Component componentAt;
            int pos = ls_context.getItemPosByVisibleIndex(0);
            ls_header.scrollTo(0);
            if (pos >= 0 && pos < bean.size()) {
                String group = bean.get(pos).getInfo().getGroup();
                for (int i4 = 0; i4 < stringList.size(); i4++) {
                    if (stringList.get(i4).getInfo().getGroup().equals(bean.get(pos).getInfo().getGroup())) {
                        if (api.getComponent() != null) {
                            ls_header.scrollTo(i4);

                            api.getComponent().setBackground(shapeElement1);
                            // 跑马灯效果
                            ((Text) api.getComponent().findComponentById(ResourceTable.Id_tv_group)).setTruncationMode(Text.TruncationMode.AUTO_SCROLLING);
                            // 始终处于自动滚动状态
                            ((Text) api.getComponent().findComponentById(ResourceTable.Id_tv_group)).setAutoScrollingCount(Text.AUTO_SCROLLING_FOREVER);
                            // 启动跑马灯效果
                            ((Text) api.getComponent().findComponentById(ResourceTable.Id_tv_group)).stopAutoScrolling();
                            // 设置text颜色
                            ((Text) api.getComponent().findComponentById(ResourceTable.Id_tv_group)).setTextColor(Color.GRAY);
                        }
                        componentAt = ls_header.getComponentAt(i4);
                        componentAt.setBackground(shapeElement2);
                        ((Text) componentAt.findComponentById(ResourceTable.Id_tv_group)).startAutoScrolling();
                        ((Text) componentAt.findComponentById(ResourceTable.Id_tv_group)).setTextColor(Color.WHITE);
                        api.setComponent(componentAt);
                        break;
                    }
                }

                tv_heard.setText(group);
                if (pos > 0) {
                    tv_heard.setVisibility(Component.VISIBLE);
                } else {
                    tv_heard.setVisibility(Component.HIDE);
                }
            }
        });

        linkageSecondaryAdapter.setOnClickMyTextView(new LinkageSecondaryDialogAdapter.onClickMyTextView() {
            @Override
            public void myTextViewClick() {
                dl_bottomsheet.setBackground(element2);
                dissMiss();
            }
        });

        ls_context.setTouchEventListener(new Component.TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                mTouchEvent.setEvent(touchEvent);
                mVelocityDetector.addEvent(mTouchEvent);
                switch (touchEvent.getAction()) {
                    case TouchEvent.PRIMARY_POINT_DOWN:
                        mLastX = touchEvent.getPointerScreenPosition(0).getX();
                        mLastY = touchEvent.getPointerScreenPosition(0).getY();
                        return true;
                    case TouchEvent.POINT_MOVE:
                        float mCurX = touchEvent.getPointerScreenPosition(0).getX();
                        float mCurY = touchEvent.getPointerScreenPosition(0).getY();
                        float offsetY = mCurY - mLastY;
                        if (Math.abs(mCurX - mLastX) < Math.abs(mCurY - mLastY)) {
                            if (offsetY > 0) {
                                // 下拉
                                if (!ls_context.canScroll(Component.DRAG_DOWN)) {
                                    ls_context.setEnabled(false);
                                    directionalLayout.setTranslationY(directionalLayout.getTranslationY() + offsetY);
                                } else {
                                    ls_context.setEnabled(true);
                                }
                            } else {
                                // 上拉
                                if (directionalLayout.getTranslationY() > 0) {
                                    ls_context.setEnabled(false);
                                    directionalLayout.setTranslationY(directionalLayout.getTranslationY() + offsetY);
                                } else {
                                    directionalLayout.setTranslationY(0);
                                    ls_context.setEnabled(true);
                                }
                            }
                            mLastX = mCurX;
                            mLastY = mCurY;
                            return true;
                        }
                        break;
                    case TouchEvent.PRIMARY_POINT_UP:
                        mVelocityDetector.calculateCurrentVelocity(1000);
                        if (mVelocityDetector.getVerticalVelocity() > 1500 && !ls_context.canScroll(Component.DRAG_DOWN)) {
                            collapseListContainer();
                        } else {
                            resetListContainer();
                        }
                        mVelocityDetector.clear();
                        // 手指释放后的逻辑处理
                        ls_context.setEnabled(true);

                        break;
                    case TouchEvent.CANCEL:
                        break;
                }
                return false;
            }
        });
    }

    private void resetListContainer() {
        int mHeight = directionalLayout.getHeight();
        float sumTranslationY = directionalLayout.getTranslationY();
        boolean isAnimUp = (sumTranslationY / mHeight) < 0.5f;
        if (sumTranslationY > 0) {
            AnimatorValue animatorValue = new AnimatorValue();
            animatorValue.setDuration(150);
            animatorValue.setCurveType(Animator.CurveType.LINEAR);
            animatorValue.setValueUpdateListener((animatorValue1, v) -> {
                if (isAnimUp) {
                    directionalLayout.setTranslationY((1 - v) * sumTranslationY);
                } else {
                    directionalLayout.setTranslationY(sumTranslationY + v * (mHeight - sumTranslationY));
                }
            });
            animatorValue.setStateChangedListener(new AnimStateChangedListener() {
                @Override
                public void onEnd(Animator animator) {
                    if (!isAnimUp) {
                        dl_bottomsheet.setBackground(element2);
                        buttonDissmiss.setVisibility(Component.VISIBLE);
                        directionalLayout.setVisibility(Component.HIDE);
                    }
                }
            });
            animatorValue.start();
        }
    }

    private void collapseListContainer() {
        int mHeight = directionalLayout.getHeight();
        float sumTranslationY = directionalLayout.getTranslationY();
        AnimatorValue animatorValue = new AnimatorValue();
        animatorValue.setDuration(150);
        animatorValue.setCurveType(Animator.CurveType.LINEAR);
        animatorValue.setValueUpdateListener((animatorValue1, v) -> {
            directionalLayout.setTranslationY(sumTranslationY + v * (mHeight - sumTranslationY));
        });
        animatorValue.setStateChangedListener(new AnimStateChangedListener() {
            @Override
            public void onEnd(Animator animator) {
                dl_bottomsheet.setBackground(element2);
                buttonDissmiss.setVisibility(Component.VISIBLE);
                directionalLayout.setVisibility(Component.HIDE);
            }
        });
        animatorValue.start();
    }

    /**
     * 滑动展示
     */
    public void show() {
        if (animationIn == null) {
            animationIn = new AnimatorValue();
            animationIn.setDuration(500);
            animationIn.setCurveType(Animator.CurveType.LINEAR);
            animationIn.setValueUpdateListener((animatorValue, v) -> {
                directionalLayout.setVisibility(Component.VISIBLE);
                directionalLayout.setTranslationY((1 - v) * directionalLayout.getHeight());
            });
        }
        animationIn.start();
    }

    /**
     * dissMiss下滑滑动
     */
    public void dissMiss() {
        if (animationOut == null) {
            animationOut = new AnimatorValue();
            animationOut.setDuration(500);
            animationOut.setCurveType(Animator.CurveType.ACCELERATE_DECELERATE);
            animationOut.setValueUpdateListener((animatorValue, v) -> {
                if (v == 1) {
                    buttonDissmiss.setVisibility(Component.VISIBLE);
                    directionalLayout.setVisibility(Component.HIDE);
                }
                directionalLayout.setTranslationY(v * directionalLayout.getHeight());
            });
        }
        animationOut.start();
        isFlag = !isFlag;
    }

    private double getTouchX(TouchEvent event, int index, Component component) {
        double x = 0;
        if (event.getPointerCount() > index) {
            int[] xy = component.getLocationOnScreen();
            if (xy != null && xy.length == 2) {
                x = event.getPointerScreenPosition(index).getX() - xy[0];
            } else {
                x = event.getPointerPosition(index).getX();
            }
        }
        return x;
    }

    private double getTouchY(TouchEvent event, int index, Component component) {
        double y = 0;
        if (event.getPointerCount() > index) {
            int[] xy = component.getLocationOnScreen();
            if (xy != null && xy.length == 2) {
                y = event.getPointerScreenPosition(index).getY() - xy[1];
            } else {
                y = event.getPointerPosition(index).getY();
            }
        }
        return y;
    }

    private void initButtonClick(String text) {
        ToastDialog toastDialog = new ToastDialog(getContext());
        toastDialog.setSize(ComponentContainer.LayoutConfig.MATCH_CONTENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
        Component parse = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_layout_toast_button, null, false);
        toastDialog.setComponent(parse);
        toastDialog.setTransparent(true);
        ((Text) parse.findComponentById(ResourceTable.Id_msg_toast)).setText(text);
        toastDialog.show();
    }

    private void clickElement(Button button) {
        getUITaskDispatcher().delayDispatch(() -> {
            ShapeElement element = new ShapeElement();
            element.setRgbColor(new RgbColor(111, 10, 238));
            element.setCornerRadius(15);
            button.setBackground(element);
        }, 100);
        ShapeElement element = new ShapeElement();
        element.setRgbColor(new RgbColor(55, 0, 179));
        element.setCornerRadius(15);
        button.setBackground(element);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onSelected(TabList.Tab tab) {
        if (pageSlider.getCurrentPage() != tab.getPosition()) {
            pageSlider.setCurrentPage(tab.getPosition());
        }
        int width = 0;
        api.setComponent(null);
        for (int index = 0; index < tab.getPosition(); index++) {
            width += tabList.getTabAt(index).getWidth();
        }
        tabList.scrollTo(width, 100);
    }

    @Override
    public void onUnselected(TabList.Tab tab) {
    }

    @Override
    public void onReselected(TabList.Tab tab) {
    }

    @Override
    public void onPageSliding(int i, float v, int i1) {
    }

    @Override
    public void onPageSlideStateChanged(int i) {
    }

    @Override
    public void onPageChosen(int position) {
        if (tabList.getSelectedTab().getPosition() != position) {
            tabList.selectTabAt(position);
            tabList.scrollTo(350 * (position - 1), (int) tabList.getSelectedTab().getContentPositionY());
        }
        api.setComponent(null);
        selectPage(position);
    }

    private void selectPage(int position) {
        switch (position) {
            case 0:
                initRxMagic();
                break;
            case 1:
                initEleme();
                break;
            case 2:
                initSwitch();
                break;
            case 3:
                initCustomHeader();
                break;
            case 4:
                initCustomDecoration();
                break;
            case 5:
                initCustomDlaLog();
                break;
            case 6:
                initBottomSheet();
                break;
            case 7:
                initMuchPrimary();
                break;
            case 8:
                initLessPrimary();
                break;
            default:
                initRxMagic();
                break;
        }
    }
}

