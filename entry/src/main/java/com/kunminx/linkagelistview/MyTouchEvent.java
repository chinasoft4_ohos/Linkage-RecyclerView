/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kunminx.linkagelistview;

import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

public class MyTouchEvent extends TouchEvent {
    private TouchEvent event;

    public void setEvent(TouchEvent event) {
        this.event = event;
    }

    @Override
    public int getAction() {
        return event.getAction();
    }

    @Override
    public int getIndex() {
        return event.getIndex();
    }

    @Override
    public long getStartTime() {
        return event.getStartTime();
    }

    @Override
    public int getPhase() {
        return event.getPhase();
    }

    @Override
    public MmiPoint getPointerPosition(int i) {
        return event.getPointerScreenPosition(i);
    }

    @Override
    public void setScreenOffset(float v, float v1) {
        event.setScreenOffset(v, v1);
    }

    @Override
    public MmiPoint getPointerScreenPosition(int i) {
        return event.getPointerScreenPosition(i);
    }

    @Override
    public int getPointerCount() {
        return event.getPointerCount();
    }

    @Override
    public int getPointerId(int i) {
        return event.getPointerId(i);
    }

    @Override
    public float getForce(int i) {
        return event.getForce(i);
    }

    @Override
    public float getRadius(int i) {
        return event.getRadius(i);
    }

    @Override
    public int getSourceDevice() {
        return event.getSourceDevice();
    }

    @Override
    public String getDeviceId() {
        return event.getDeviceId();
    }

    @Override
    public int getInputDeviceId() {
        return event.getInputDeviceId();
    }

    @Override
    public long getOccurredTime() {
        return event.getOccurredTime();
    }
}